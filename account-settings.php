<?php 
    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });

    // create database connection
    $db = Db::getInstance();

    if(isset($_POST['avatarSubmit'])) {
        try {
            // assign avatar to the user
            $avatar = new User($db);
            $avatar->setImage($_FILES['avatar']);
            $avatar->setAvatar();
            // resize and crop de image from center (100px width and 100px height standard)
            $avatar->resizeAvatar(); // (width, height, quality)
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }

    if(isset($_POST['deleteSubmit'])) {
        try {
            $deleteAvatat = new User($db);
            $deleteAvatat->deleteAvatar();
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }

    if(isset($_POST['profileChangeSubmit'])) {
        try {
            $changeProfile = new User($db);
            $changeProfile->setFullname($_POST['user-name']);
            $changeProfile->setEmail($_POST['user-email']);
            $changeProfile->setBio($_POST['user-bio']);
            $changeProfile->updateProfile();
            $success = "Profile is now updated";
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }

    if(isset($_POST['passwordChangeSubmit'])) {
        try {
            $changePassword = new User($db);
            $changePassword->setPassword($_POST['user-old-password']);

            $security = new Security();
            $security->password = $_POST['user-password'];
            $security->passwordConfirmation = $_POST['user-password-confirmation'];
            if($security->passwordsAreSecure() ){
                $changePassword->setNewPassword($_POST['user-password']);
                $changePassword->updatePassword();
                $success = "Password is now updated";
            }
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }

    $actualUser = new User($db);
    $userResults = $actualUser->getUserResults($_SESSION['username']);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Account - Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    
    <?php
        include_once("includes/header.inc.php");
    ?>

    <?php if(!empty($error)): ?>
        <div class="profile__change__msg profile__change__msg--error">
            <p><?php echo $error ?></p>
        </div>
    <?php endif; ?>

    <?php if(!empty($success)): ?>
        <div class="profile__change__msg profile__change__msg--success">
            <p><?php echo $success ?></p>
        </div>
    <?php endif; ?>

    <!-- Main container -->
    <main class="profile__change">
        <!-- Avatar Settings -->
        <div class="avatar__preview">
            <picture>
                <?php 
                    if($userResults['avatar_status'] == 0) {
                        echo "<img class='avatar' src='users/avatars/default-profile.png'>";
                    } else {
                        echo User::getAvatar();
                    }
                ?>
            </picture>
            <form class="avatar__preview__change" action="" method="post"> 
                <a class="preview__change__btn preview__change__btn--action preview__change__btn--open preview__change__btn--margin" href="#">Upload new picture</a>
                <!-- in comments for development <button class="preview__change__btn preview__change__btn--danger preview__change__btn--margin" type="submit" name="deleteSubmit">Delete</button> -->
            </form>
            <form class="avatar__form" action="" method="post" enctype='multipart/form-data'>
                <fieldset class="upload">
                    <input type="file" name="avatar" id="avatar">
                    <p class="form__message">JPG or PNG. Max size of 800K</p>
                </fieldset>
                <button class="preview__change__btn preview__change__btn--action preview__change__btn--upload" type="submit" name="avatarSubmit">Upload Now</button>
            </form>
        </div>
        <div class="settings__menu">
            <a class="user__settings selected" href="#">User settings</a>
            <a class="password__change" href="#">Change Password</a>
        </div>
        <!-- Profile Informations Settings -->
        <div class="user__preview">
            <form class="edit__user" action="" method="post">
                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-name">Name</label>
                        <input type="text" value="<?php echo htmlspecialchars($userResults['fullname']) ?>" name="user-name" id="user-name">
                    </fieldset>
                    <p class="form__message">We’re big on real names around here, so people know who’s who</p>
                </div>
                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-email">Email</label>
                        <input type="text" value="<?php echo htmlspecialchars($userResults['email']) ?>" name="user-email" id="user-email">
                    </fieldset>
                </div>
            
                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-bio">Bio</label>
                        <textarea name="user-bio" id="user-bio"><?php echo htmlspecialchars($userResults['bio']) ?></textarea>
                    </fieldset>
                    <p class="form__message">URLs are hyperlinked.</p>
                </div>
            
                <button class="preview__change__btn preview__change__btn--action" type="submit" name="profileChangeSubmit">Save</button>
            </form>
        </div>
        <div class="password__preview">
            <form class="edit__password" action="" method="post">
                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-old-password">Old password</label>
                        <input type="password" name="user-old-password" id="user-old-password">
                    </fieldset>
                </div>

                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-password">New Password</label>
                        <input type="password" name="user-password" id="user-password">
                    </fieldset>
                </div>

                <div class="form-field">
                    <fieldset class="edit__field">
                        <label for="user-password-confirmation">New Password Confirmation</label>
                        <input type="password" name="user-password-confirmation" id="user-password-confirmation">
                    </fieldset>
                    <p class="form__message">Minimum 8 characters</p>
                </div>

                <button class="preview__change__btn preview__change__btn--action" type="submit" name="passwordChangeSubmit">Change Password</button>
            </form>
        </div>
    </main>
    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>