<?php
    # color-extractor
    //require_once(dirname(__FILE__) . '/vendor/autoload.php');

    use League\ColorExtractor\Color;
    use League\ColorExtractor\ColorExtractor;
    use League\ColorExtractor\Palette;

    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });

    // Check if logged in
    User::authenticate();
    
    if(isset($_GET['id'])) {
        // create database connection
        $db = Db::getInstance();
        // get post id
        $postId = $_GET['id'];
        // get user informations to use on posts (username, avatar, etc.)
        $user = new User($db);
        $userResults = $user->getUserResults($_SESSION['username']);
        $post = new Post($db);
        $postResults = $post->getOne($postId);
        
        //load comment
        $comment = new Comment($db);
        $allComments = $comment->getAllComments($postId);
        $totalComments = $comment->countComments($postId);

        //total likes 
        $totalLikes = $post->countLikes($postId);

        //total reports
        $total = $post->countReports($postId);

        // colors
        $colors = $post->fetchColors($postId);

        // filter
        $filter = $post->showFilter($postId);

        // check if user has clicked on delete post
        if(isset($_GET['delete'])) {
            // check if user can delete post
            if(($_GET['delete'] == $_GET['id']) && ($postResults['username'] == $_SESSION['username'])) {
                $post->deletePost($postId);
                header("Location: user.php?user=".$_SESSION['username']);
            }
        }
    } else {
        header("Location: index.php");
    }

    //location
    $location = $post->getCityName($postId);

    //related posts
    $poster = $postResults['user_id'];
    $relatedPosts = $post->getRelatedPosts($poster, $postId);
    $numberOfPosts = count($relatedPosts);

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="wrapper">
    <!-- navigation -->
    <?php
        include_once("includes/header.inc.php");
    ?>

    <!-- main container -->
    <?php if($total<3): ?>
    <main class="sight__container">
    <div>
        <div class="sight__userInformation">
            <?php if($postResults['avatar_status']==1): ?>
                <img class="userInformation__avatar" src="<?php echo $postResults['avatar'] ?>" alt="avatar">
            <?php else: ?>
                <img class="userInformation__avatar" src="users/avatars/default-profile.png" alt="avatar">
            <?php endif; ?>
            <h1 class= "userInformation__username"> <?php echo $postResults['username'] ?> </h1>
            <p class="userInformation__subtle">
                <span class="userInformation__subtle--lighter">Published at </span><?php echo $location['city']?>
                <span class=userInformation__subtle--lighter>on </span><?php echo date("j F Y", strtotime($postResults['post_time'])) ?>
            </p>
        </div>

        <div class="sight__postInformation">
            <!-- apply filter to image -->
            <figure class="<?php echo $filter ?>">
                <img class="postInformation__post" src="<?php echo $postResults['image'] ?>" alt="post">
            </figure>

            <!-- edit/delete icons -->
            <div class="postSettings">
                <?php if ($postResults['username'] == $_SESSION['username']): ?>
                    <div class="post__settings">
                        <a href="edit-post.php?id=<?php echo $postId; ?>" class="edit__post"><img src="assets/icons/edit.svg" alt="edit icon">Edit</a>
                    </div>
                    <div class="post__delete">
                        <a href="?id=<?php echo $postId; ?>&delete=<?php echo $postId; ?>" class="edit__post"><img src="assets/icons/close.svg" alt="edit icon">Delete</a>
                    </div>
                <?php endif; ?>
            </div>

            <!-- report post -->
            <?php if($userResults['id'] != $postResults['user_id']): ?>
                <div class="report">
                    <?php if($post->isReported($userResults['id'], $postId)== 0): ?>
                        <a href="#" id="<?php echo $postId; ?>" class="report__post toReport">
                            <p>Mark as inappropriate.</p>
                        </a>
                        <?php else: ?>
                        <a href="#" id="<?php echo $postId; ?>" class="report__post">
                            <p>Thank you for reporting this post. Undo here.</p>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <!-- post information -->
            <div class="postInformation__description">
                <p><?php echo Post::returnLinkFromTag(htmlspecialchars($postResults['description'])); ?></p>
            </div>
            <ul class="shotStats">
                <!-- colorpalette -->
                <li class="shotStats_colors">
                    <span class="stats-action">
                        <img src="assets/icons/colors.svg" alt="color drop icon">
                    </span>
                    <ul class="color-palette group">
                        <?php foreach ($colors as $color):?>
                            <!-- apply filter to colorpalette -->
                            <li class="color <?php echo $filter ?>" style="background-color:<?php echo $color['hex']; ?>">
                                <!-- remove # from hexcode to be able to fetch color in GET-request -->  
                                <a href="search.php?color=<?php $searchColor = str_replace('#', '', $color['hex']); echo $searchColor; ?>"></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <!-- likes -->
                <li class="shotStats_likes">
                    <span class="stats-action">
                        <img src="assets/icons/like.svg" alt="like icon">
                    </span>
                    <div class="stats-count">
                        <p><?php echo $totalLikes ?> <span> like(s)</span></p>
                    </div>
                </li>
            </ul>
        </div>
          
        <!-- comment section -->
        <section class="comments">
            <?php if($totalComments > 1 || $totalComments == 0): ?>
                <?php echo "<h3><span class='comment__count'>". $totalComments ."</span> comments</h3>" ?>
            <?php else: ?>
                <?php echo "<h3><span class='comment__count'>". $totalComments ."</span> comment</h3>" ?> 
            <?php endif; ?>

            <?php if(count($allComments) == 0): ?>
                <li class="comments__noComment">
                    <p>Be the first to write a comment on <span class="comments__noComment--accent"><?php echo $postResults['username'] ?></span>'s post!</p>
                </li>
            <?php endif; ?>

            <!-- add comment form -->
            <form action="" method="post" class="writeComment__form">  
                <input type="text" placeholder="Add a comment" class="writeComment__input" name="comment" id="comment" cols="30" rows="1"></input>  
                <input class="writeComment__send__btn writeComment__send__btn--action" type=submit name=btnComment id="btnComment" data-post="<?php echo $postId; ?>" value=Post>
            </form>
         
            <!-- list of comments -->
            <ul class="comments__list">
                <?php if(count($allComments) > 0): ?>
                    <?php foreach($allComments as $comment){ ?>
                    <li class="comments__comment">
                        <?php if($comment['avatar_status'] == 0) {
                            echo "<img class='comments__comment__avatar' src='users/avatars/default-profile.png' alt='avatar'>";
                        } else {
                            echo "<img class='comments__comment__avatar' src='".$comment['avatar']."' alt='avatar'>";
                        }
                        ?>
                        <div class="comments__text">
                            <p class="comments__comment__username"><?php echo $comment['username'] ?></p>
                            <p class="comments__comment__date"><?php echo Post::timeAgo($comment['date_posted']);; ?></p>
                            <p class="comments__comment__text"><?php echo htmlspecialchars($comment['text']); ?> </p>
                            
                        </div>
                    </li>
                    <?php } ?>
                <?php endif; ?>
            </ul>
        </section>

        <!-- related posts section -->
        <?php if($numberOfPosts > 1): ?>
            <section class="relatedPosts__container">
                <h3>More from <?php echo $postResults['username']; ?></h3>
                <div class="relatedPosts">
                <?php foreach($relatedPosts as $relatedPost): ?>
                        <?php 
                            $relatedPostFilter = $post->showFilter($relatedPost['id']);
                        ?>
                        <a href="sight.php?id=<?php echo $relatedPost['id'] ?>" class="relatedPosts__link"><img class='relatedPosts__link__img <?php echo $relatedPostFilter ?>' src="<?php echo $relatedPost['thumb_image']; ?>" alt="post"></a>
                <?php endforeach; ?>
                </div>
            </section>
        <?php endif; ?>
    </main>
    <?php endif; ?>
    </div>

    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>