<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });
 
    // Check if logged in
    User::authenticate();
   
    // create database connection
    $db = Db::getInstance();
    // get post id
    $postId = $_GET['id'];
    // get user informations to use on posts (username, avatar, etc.)
    $user = new User($db);
    $userResults = $user->getUserResults($_SESSION['username']);
    $userId = $user->getUserId($_SESSION['username']);
    $post = new Post($db);
   
    if(isset($_POST['postEditSubmit'])) {
        try {
            $post->setDescription($_POST['description']);
            $post->saveTags();
            $newTags = $post->findTags();
            $post->editPostTags($newTags, $postId);
            $post->updatePost($postId);
            $success = "Post is now updated";
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }
    $postTags = $post->getAllTags($postId);
    $tags = "";
    foreach($postTags as $tag) {
        $tags.= $tag . ", ";
    }
    $postResults = $post->getOne($postId);
 
    if($postResults['username'] !== $_SESSION['username']) {
        header('Location: index.php');
    }
    ?><!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Account - Pinsight</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php include_once("includes/header.inc.php"); ?>
   
        <?php if(!empty($error)): ?>
            <div class="profile__change__msg profile__change__msg--error">
                <p><?php echo $error ?></p>
            </div>
        <?php endif; ?>
   
        <?php if(!empty($success)): ?>
            <div class="profile__change__msg profile__change__msg--success">
                <p><?php echo $success ?></p>
            </div>
        <?php endif; ?>
 
        <div class="container">
            <div class="container__header">
                <h1>Edit this post</h1>
            </div>
 
            <!-- Main container -->
            <main class="upload">
                <!-- Avatar Settings -->
                <div>
                    <img src="<?php echo $postResults['image']; ?>" alt="<?php echo $postResults['title']; ?>">
                </div>
                <!-- Post Settings -->
                <div>
                    <form class="upload__form" action="" method="post">
                        <div class="form-field">
                            <fieldset class="edit__field">
                                <label for="description"></label>
                                <textarea name="description" id="description"><?php echo htmlspecialchars($postResults['description']); ?></textarea>
                            </fieldset>
                        </div>
                        <button class="preview__change__btn preview__change__btn--action edit__result" type="submit" name="postEditSubmit">Save changes</button>
                    </form>
                </div>
   
            </main>
        </div>
    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    </body>
    </html>