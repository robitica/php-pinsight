$(document).ready(function(){
 
    //# SHOW PROFILE UPLOAD AVATAR BUTTON #//
    $(".preview__change__btn--open").on("click", function(e) {
        $(this).hide();
        $(".avatar__form").show();
        e.preventDefault();
    });

    //# SHOW PROFILE UPLOAD AVATAR BUTTON #//
    if(document.querySelector('.password__preview')) {
        $(".password__preview").hide();
        $(".settings__menu a").on("click", function(e) {
            if($(".user__settings").hasClass("selected")) {
                $(".user__settings").removeClass("selected");
                $(".password__change").addClass("selected");
                $(".user__preview").hide()
                $(".password__preview").show();
            } else {
                $(".password__change").removeClass("selected");
                $(".user__settings").addClass("selected");
                $(".password__preview").hide()
                $(".user__preview").show();
            }
            e.preventDefault();
        });
    }
 
    //# PREVIEW IMAGE #//
    var p = $("#filePreview");
    $("#fileInput").change(function(){
        // fadeout or hide preview
        p.fadeOut();
        //$(".filters").fadeOut();
 
        // prepare HTML5 FileReader
        var Reader = new FileReader();
        Reader.readAsDataURL(document.getElementById("fileInput").files[0]);
 
        Reader.onload = function (event) {
            p.attr('src', event.target.result).fadeIn();
            p.css('display', 'block');
            $(".filter__preview").attr('src', event.target.result).fadeIn();
            $(".filters").css('visibility', 'visible');
            $('label.fileInput').css('display', 'none');
        };
    });
 
    //# SLIDER FOR FILTER SCROLLING #//
    if(document.querySelector(".filters")) {
        $(".filters").slick({
            arrows: false,
            slidesToShow: 8,
            infinite: false,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    dots: false,
                    slidesToShow: 8,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }, {
                breakpoint: 600,
                settings: {
                    dots: true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }, {
                breakpoint: 375,
                settings: {
                    dots: true,
                    centerMode: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }]
        });
    }
 
    //# PREVIEW APPLIED FILTER #//
    var filters = document.getElementsByName("filter");
    $(".filter").on('click', function(e){
        e.preventDefault();
        $(":input", this).prop('checked', true);
        var filter_value;
        for (var i = 0; i < filters.length; i++) {
            if(filters[i].checked) {
                filter_value = filters[i].value
            }
        }
 
        $.ajax({
            method: "POST",
            url: "ajax/applyFilter.php",
            data: {filtername: filter_value}
        })
        .done(function(res){
            if(res.status == "success") {
                for (var i = 0; i < filters.length; i++) {
                    if(filters[i].checked) {
                        $(".post__preview figure").removeClass().addClass(res.filter);
                    }
                }
            }
        })
    })
 
    //# LOAD MORE POSTS #//
    var postsOnPage = 20;
    var postsToBeAdded = 20;
    $(".load__more__btn").click(function(e) {
        $.ajax({
            method: "POST",
            url: "ajax/load-posts.ajax.php",
            data: {
                page: $(this).attr('id'),
                newPostsOnPage: postsOnPage,
                newPostsToBeAdded: postsToBeAdded
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                // hide load more if there are no more posts available
                if(res.hideLoadMore == true) {
                    $(".load__more").hide();
                }
                postsOnPage = postsOnPage + postsToBeAdded;
                for (var i = 0, len = res.posts.length; i < len; i++) {
                    function getUserFromNewPost() {
                        if(res.posts[i].avatar !== "") {
                            return `<img src="${res.posts[i].avatar}" alt="${res.posts[i].username}">`;
                        } else {
                            return `<img src="users/avatars/default-profile.png" alt="default avatar">`;
                        }
                    }
 
                    function getLikesFromNewPost() {
                        if(res.likes[i] == 0) {
                            return `<a href="#" id="${res.posts[i].post_id}" class="like__post like">
                                        <img src="assets/icons/like.svg" alt="total likes">
                                    </a>
                                    <p id="${res.posts[i].post_id}" class="like__total">${res.totalLikes[i]}</p>
                                    `;
                        } else {
                            return `<a href="#" id="${res.posts[i].post_id}" class="like__post">
                                        <img src="assets/icons/liked.svg" alt="total likes">
                                    </a>
                                    <p id="${res.posts[i].post_id}" class="like__total">${res.totalLikes[i]}</p>
                                    `;
                        }
                    }
 
                    var newPostUser = getUserFromNewPost();
                    var newPostLikes = getLikesFromNewPost();
                    var newPost = `<li class="main__posts__item">
                        <div class="main__posts__item__header">
                            <div class="main__posts__item__header__image">
                                <a class="main__posts__item__header__image__link" href="sight.php?id=${res.posts[i].post_id}">
                                    <img src="${res.posts[i].thumb_image}" alt="post exemple">
                                </a>
                                <a class="main__posts__item__header__image__hover" href="sight.php?id=${res.posts[i].post_id}">
                                    <em class="timestamp">${moment(res.posts[i].post_time).startOf().fromNow()}</em>
                                </a>
                            </div>
                           
                            <ul class="main__posts__item__header__info">
                                <li class="main__posts__item__header__info__item"><a href="sight.php?id=${res.posts[i].post_id}"><img src="assets/icons/comment.svg" alt="total comments"></a><p>${res.totalComments[i]}</p></li>
                                <li class="main__posts__item__header__info__item">${newPostLikes}</li>
                            </ul>
                        </div>
                        <div class="main__posts__item__user">
                            ${newPostUser}
                            <a href="user.php?user=${res.posts[i].username}">${res.posts[i].fullname}</a>
                        </div>
                    </li>`
                    $(".main__posts").append(newPost);
                }
 
            }
        });
        e.preventDefault();
    });
 
    //# Follow button #//
    var followStatus;
    //if follow button click run function
    $(".follow__btn").click(function(e) {
        if($(this).hasClass("follow")) {
            followStatus = "Follow";
        } else {
            followStatus = "Following";
        }
        $.ajax({
            method: "POST",
            url: "ajax/follow.ajax.php",
            data: {
                userPage: $(this).attr('id'),
                followStatus: followStatus
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                if(res.followStatus == "following") {
                    $(".follow__btn").removeClass('follow preview__change__btn--action').addClass("following preview__change__btn--danger");
                    $(".follow__btn").text("Following");
                } else {
                    $(".follow__btn").removeClass('following preview__change__btn--danger').addClass("follow preview__change__btn--action");
                    $(".follow__btn").text("Follow");
                }
                $(".follow__btn").removeAttr('id').attr("id", res.followerUsername);
            }
 
        });
        e.preventDefault();
    });
 
    //# Friend button #//
    var friendStatus;
    //if friend button click run function
    $(".friend__btn").click(function(e) {
        if($(this).hasClass("friend__btn--add")) {
            friendStatus = "add-friend";
        } else if($(this).hasClass("friend__btn--accept")) {
            friendStatus = "accept-friend";
        } else if ($(this).hasClass("friend__btn--unfriend")) {
            friendStatus = "unfriend";
        }
        $.ajax({
            method: "POST",
            url: "ajax/friend.ajax.php",
            data: {
                inspectedUser: $(this).attr('id'),
                friendStatus: friendStatus
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                if(res.friendStatus == "pending") {
                    $(".friend__btn").removeClass('friend__btn--add').addClass("friend__btn--pending");
                    $(".friend__btn").text("Pending");
                } else if(res.friendStatus == "unfriend"){
                    $(".friend__btn").removeClass('friend__btn--accept').addClass("friend__btn--unfriend");
                    $(".friend__btn").text("Unfriend");
                } else if(res.friendStatus == "add-friend"){
                    $(".friend__btn").removeClass('friend__btn--unfriend').addClass("friend__btn--add");
                    $(".friend__btn").text("Add Friend");
                }
                $(".follow__btn").removeAttr('id').attr("id", res.friendUsername);
            }
 
        });
        e.preventDefault();
    });
 
    var likeStatus;
    var postId;
    //if like click run function
    $(".main__posts").on('click', '.like__post', function(e) {
        postId = $(this).attr('id');
        if($(this).hasClass("like")) {
            likeStatus = "liked";
        } else {
            likeStatus = "like";
        }
        $.ajax({
            method: "POST",
            url: "ajax/like.ajax.php",
            data: {
                likeStatus: likeStatus,
                postId: postId
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                if(res.likeStatus == "liked") {
                    var likeTotal = parseInt($("#"+res.postId+ ".like__total").text());
                    likeTotal = likeTotal+1;
                    $("#"+res.postId+ ".like__total").text(likeTotal);
                    $("#"+res.postId).removeClass('like');
                    $("#"+res.postId).children("img").attr("src","assets/icons/liked.svg");
                } else {
                    $("#"+res.postId).addClass("like");
                    $("#"+res.postId).children("img").attr("src","assets/icons/like.svg");
                    var likeTotal = parseInt($("#"+res.postId+ ".like__total").text());
                    likeTotal = likeTotal-1;
                    $("#"+res.postId+ ".like__total").text(likeTotal);
                }
            }
        });
        e.preventDefault();
    });
 
   $("#btnComment").on("click", function(e){
    var comment = $("#comment").val();
    var postId = $("#btnComment").data("post");
   
    //perform HTTP (ajax) request
    $.ajax({
        method: "POST",
        url: "ajax/comment.ajax.php",
        data: { comment : comment, postId : postId }
      })
        //when done append comment to li 
        .done(function(res) {
            if(res.status == "success" ){
                if(res.avatarStatus == 1){
                    var newComment = `<li style='display:none' class="comments__comment">
                    <img class='comments__comment__avatar' src='${res.image}' alt='avatar'>
                    <div class="comments__text">            
                    <p class="comments__comment__username">${res.username}</p>
                    <p class="comments__comment__text">${res.comment}</p>
                    <p class="comments__comment__date">just now</p>
                    </div>
                    </li>`
                }
                else{
                    var newComment = `<li style='display:none' class="comments__comment">
                    <img class='comments__comment__avatar' src='users/avatars/default-profile.png' alt='avatar'>
                    <div class="comments__text">            
                    <p class="comments__comment__username">${res.username}</p>
                    <p class="comments__comment__text">${res.comment}</p>
                    <p class="comments__comment__date">just now</p>
                    </div>
                    </li>`
                }
               
                var commentCount = $(".comment__count").text();
                commentCount = parseInt(commentCount);
                $(".comment__count").text(commentCount+1);
                $(".comments ul").prepend(newComment);
                $('.comments ul li').first().slideDown();
                //empty value when posted
                $(".writeComment__form .writeComment__input").val("");
                //delete 'no comments yet' 
                $(".comments__noComment").hide();
            }
        });
   
    e.preventDefault(); //to prevent sending form
   });
 
   //# REPORT POST #//
   var reportStatus;
   var postId;
   $(".report a").on("click", function(e){
       //get post id
        postId = $("#btnComment").data("post");
 
        //check if post is reported by current user
        if($(this).hasClass("toReport")) {
            reportStatus = "reported";
        }
        else{
            reportStatus = "report";
        }
 
        $.ajax({
            method: "POST",
            url: "ajax/report.ajax.php",
            data: {
                reportStatus: reportStatus,
                postId: postId
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                if(res.reportStatus == "reported") {
                    var reportTotal = parseInt($("#"+res.postId+ ".report__total").text());
                    reportTotal = reportTotal+1;
                    $("#"+res.postId+ ".report__total").text(reportTotal);
                    $("#"+res.postId).removeClass('toReport');
                    $("#"+res.postId).children("p").text("Thank you for reporting this post. Undo Here.");
                } else {
                    $("#"+res.postId).addClass("toReport");
                    $("#"+res.postId).children("p").text("Marks as inappropriate.");
                    var reportTotal = parseInt($("#"+res.postId+ ".report__total").text());
                    reportTotal = reportTotal-1;
                    $("#"+res.postId+ ".report__total").text(reportTotal);
                }
            }
        });
 
    e.preventDefault();    
   });
 
   //# GET GEOLOCATION #//
 
   //coords
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };
     
      function success(pos) {
        var crd = pos.coords;
 
        var lat = crd.latitude;
        var lng = crd.longitude;
     
        //add coords to invisible form fields longitude & latitude
        $("#latitude").val(lat);
        $("#longitude").val(lng);

        // geolocaton as query parameters for index page
        $(".nearby a").attr("href", "index.php?sort=nearby&lat="+lat+"&long="+lng);
        
      }
     
      function error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
      }
     
      navigator.geolocation.getCurrentPosition(success, error, options);
      
      let storableLocation = {};
      if (window.navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
              var lat 	 = position.coords.latitude,
                  lng 	 = position.coords.longitude,
                  latlng 	 = new google.maps.LatLng(lat, lng),
                  geocoder = new google.maps.Geocoder();
              geocoder.geocode({'latLng': latlng}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                      if (results[1]) {
  
                          for (var ac = 0; ac < results[0].address_components.length; ac++) {
                              
                              var component = results[0].address_components[ac];
                                  
                              if(component.types.includes('sublocality') || component.types.includes('locality')) {
                                      storableLocation.city = component.long_name;
                                      $("#city").val(storableLocation.city);
                              }
                              else if (component.types.includes('administrative_area_level_1')) {
                                      storableLocation.state = component.short_name;
                              }
                              else if (component.types.includes('country')) {
                                      storableLocation.country = component.long_name;
                                      storableLocation.registered_country_iso_code = component.short_name;
                              }
  
                          };
                      }
                      else {console.log("No reverse geocode results.")}
                  }
                  else {console.log("Geocoder failed: " + status)}
              });
          },
          function() {console.log("Geolocation not available.")});
      }
        
        $("#city").val(storableLocation.city);
        console.log(storableLocation);
 
    //# Follow tag #//
    var followTagStatus;
    //if follow button click run function
    $(".tag_follow_btn").click(function(e) {
        if($(this).hasClass("follow")) {
            followTagStatus = "Follow";
        } else {
            followTagStatus = "Following";
        }
        $.ajax({
            method: "POST",
            url: "ajax/followtag.ajax.php",
            data: {
                tag: $(this).attr('id'),
                followStatus: followTagStatus
            }
        })
        .done(function(res) {
            if(res.status == "success") {
                if(res.followTagStatus == "following") {
                    $(".tag_follow_btn").removeClass('follow').addClass("following");
                    $(".tag_follow_btn").text("Following");
                } else {
                    $(".tag_follow_btn").removeClass('following').addClass("follow");
                    $(".tag_follow_btn").text("Follow");
                }
            }
 
        });
        e.preventDefault();
    });
 
 
 });