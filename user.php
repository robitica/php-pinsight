<?php

    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });

    // Check if logged in
    User::authenticate();

    if(isset($_GET['user'])) {
        // get inspected user username
        $inspectedUser = $_GET['user'];
        // create database connection
        $db = Db::getInstance();
        // get users information
        $user = new User($db);
        $userResults = $user->getUserResults($_SESSION['username']);
        $inspectedUserResults = $user->getUserResults($inspectedUser); 
        // if inspected user don't exists go back to index.php
        if($inspectedUserResults == false) {
            header("Location: index.php");
        }
        // get posts of the inspected user
        $posts = new Post($db);
        $inspectedUserPosts = $posts->getAllFromUser($inspectedUser, 1);
        // get actual user id and friend user id
        $userOne = $user->getUserId($_SESSION['username']);
        $userTwo = $user->getUserId($inspectedUser);
        // get relationship
        $actualRelationship = new Relationship($db);
        $actualRelationship->setUserOne($userOne);
        $actualRelationship->setUserTwo($userTwo);
        if($actualRelationship->getRelationshipStatus() == 1 || $userOne == $userTwo) {
            $inspectedUserPosts = $posts->getAllFromUser($inspectedUser, 3);
        }
        // get following button
        $userFollow = new Follow($db);
        $senderId = $userFollow->getUserId($_SESSION['username']);
        $receiverId = $userFollow->getUserId($inspectedUser);
        //count comments
        $comment = new Comment($db);
    } else {
        header("Location: index.php");
    }

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinsight | <?php echo $inspectedUserResults['username'] ?></title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
</head>
<body>
    <div id="wrapper">
    <!-- Navigatie -->
    <?php
        include_once("includes/header.inc.php");
    ?>
    <!-- Secondary menu -->
    <nav class="secondary__nav">
        <ul>
            <li class="secondary__nav__item secondary__nav__item--selected"><a href="#">Sights</a></li>
        </ul>
    </nav>
    <!-- Main container -->
    <main class="user__profile">
        <div class="user__details">
            <div class="avatar__preview avatar__preview--user" class="user__details__avatar">
                <picture>
                    <?php 
                        if($inspectedUserResults['avatar_status'] == 0) {
                            echo "<img class='avatar' src='users/avatars/default-profile.png'>";
                        } else {
                            echo "<img class='avatar' src='".$inspectedUserResults['avatar']."'>";
                        }
                    ?>
                </picture>
            </div>
            <div class="user__details__description">
                <p class="user__details__description__fullname"><?php echo $inspectedUserResults['fullname']; ?></p>
                <p class="user__details__description__username"><?php echo $inspectedUserResults['username']; ?></p>
                <p class="user__details__description__bio"><?php echo htmlspecialchars($inspectedUserResults['bio']); ?></p>
            </div>
            <?php if(!($_SESSION['username'] === $inspectedUser)): ?>
                <div class="friend">
                    <?php echo $actualRelationship->prepareFriendButton(); ?>
                </div>
                <div class="follow">
                    <?php echo $userFollow->checkIfFollowed($senderId, $receiverId); ?>
                </div>
            <?php endif; ?>
            <?php if(($_SESSION['username'] === $inspectedUser)): ?>
                <div class="user__details__settings">
                    <a href="account-settings.php"><img src="assets/icons/settings.svg" alt="settings icon"></a>
                    <a href="settings/logout.php"><img src="assets/icons/logout-mobile.svg" alt="logout"></a>
                </div>
            <?php endif; ?>
        </div>
        <div class="user__sights">
            <ul class="main__posts main__posts--user">

                <!-- Start list with posts -->
                <?php foreach ($inspectedUserPosts as $p): ?>
                <?php $totalComments = $comment->countComments($p['post_id']); ?>
                <li class="main__posts__item">
                    <div class="main__posts__item__header">
                        <div class="main__posts__item__header__image">
                            <a class="main__posts__item__header__image__link" href="sight.php?id=<?php echo $p['post_id'] ?>">
                                <figure class= <?php echo $posts->showFilter($p['post_id']) ?>>
                                    <img src="<?php echo $p['thumb_image'] ?>" alt="post exemple">
                                </figure>
                            </a>
                            <a class="main__posts__item__header__image__hover" href="sight.php?id=<?php echo $p['post_id']; ?>">
                                <em class="timestamp"> <?php Post::timeAgo($p['post_time']); ?> </em>
                            </a>
                        </div>
                        
                        <ul class="main__posts__item__header__info">
                            <li class="main__posts__item__header__info__item"><img src="assets/icons/comment.svg" alt="total comments"><p><?php echo $totalComments; ?></p></li>
                            <li class="main__posts__item__header__info__item">
                            <?php if($posts->isLiked($userResults['id'], $p['post_id'])== 0): ?>
                            <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post like">
                                <img src="assets/icons/like.svg" alt="total likes">
                            </a>
                            <?php else: ?>
                            <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post">
                                <img src="assets/icons/liked.svg" alt="total likes">
                            </a>
                            <?php endif; ?>
                                <p id="<?php echo $p['post_id']; ?>" class="like__total">
                                    <?php 
                                        $likes = new Post($db);
                                        $total = $likes->countLikes($p['post_id']);
                                        echo $total;
                                    ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </main>
    </div>
    
    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>
