<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });
 
    // Check if logged in
    User::authenticate();
 
    // create database connection
    $db = Db::getInstance();
    // get user informations to use on posts (username, avatar, etc.)
    $user = new User($db);
    $userResults = $user->getUserResults($_SESSION['username']);
    // check if search word exists
    if (isset($_GET['search'])) {
        if(empty($_GET['search'])) {
            header('Location: index.php');
        }
        // set search key
        $searchKey = $_GET['search'];
        // create array from search words
        $searchParams = str_replace('#', '', $_GET['search']);
        $searchParams = explode(' ', $searchParams);
        $searchParams = array_map('trim', $searchParams);
        $searchParams = array_filter($searchParams, function($value) { return $value !== ''; });
        try {
            $search = new Post($db);
            $result = $search->search($searchParams);
            // get comments
            $comment = new Comment($db);
            // delete hashtag from word to check if exists (in database the hashtags are saved without #)
            $tagWithoutHashTag = str_replace('#', '', $_GET['search']);
            $checkIfTag = $search->checkIfTag($tagWithoutHashTag);
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    } elseif(isset($_GET['color'])) {
        if(empty($_GET['color'])) {
            header('Location: index.php');
        }
        // search on specific color
        try {
            // set search key
            $searchKey = "#".$_GET['color'];
            $search = new Post($db);
            // get comments
            $comment = new Comment($db);
            $result = $search->searchPostsOnColor($searchKey);
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    } else {
        header("Location: index.php");
    }

    //comments
    $comment = new Comment($db);
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
</head>
<body>
<div id="wrapper">
    <!-- Navigatie -->
    <?php
        include_once("includes/header.inc.php");
    ?>
 
    <!-- Secondary menu -->
    <nav class="secondary__nav">
        <ul>
            <?php if(isset($checkIfTag) && count($checkIfTag)): ?>
                <li class="secondary__nav__item secondary__nav__item--selected"><a href="#"><?php echo htmlspecialchars("#".$tagWithoutHashTag); ?></a></li>
                <?php echo $search->checkIfTagFollowed($userResults["id"], $checkIfTag[0]["id"]); ?>
            <?php else: ?>
                <li class="secondary__nav__item secondary__nav__item--selected"><a href="#"><?php echo htmlspecialchars($searchKey); ?></a></li>
            <?php endif; ?>
        </ul>
    </nav>
    <!-- Main container -->
    <main>
        <ul class="main__posts">

        <!-- Start list with posts -->
        <?php foreach ($result as $p): ?>
            <?php $totalComments = $comment->countComments($p['post_id']); ?>
            <li class="main__posts__item">
                <div class="main__posts__item__header">
                    <div class="main__posts__item__header__image">
                        <a class="main__posts__item__header__image__link" href="sight.php?id=<?php echo $p['post_id'] ?>">
                            <figure class= <?php echo $search->showFilter($p['post_id']) ?>>
                                <img src="<?php echo $p['thumb_image'] ?>" alt="post exemple">
                            </figure>
                        </a>
                        <a class="main__posts__item__header__image__hover" href="sight.php?id=<?php echo $p['post_id']; ?>">
                            <em class="timestamp"> <?php Post::timeAgo($p['post_time']); ?> </em>
                        </a>
                    </div>
                    
                    <ul class="main__posts__item__header__info">
                        <li class="main__posts__item__header__info__item"><img src="assets/icons/comment.svg" alt="total comments"><p><?php echo $totalComments; ?></p></li>
                        <li class="main__posts__item__header__info__item">
                        <?php if($search->isLiked($userResults['id'], $p['post_id'])== 0): ?>
                        <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post like">
                            <img src="assets/icons/like.svg" alt="total likes">
                        </a>
                        <?php else: ?>
                        <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post">
                            <img src="assets/icons/liked.svg" alt="total likes">
                        </a>
                        <?php endif; ?>
                            <p id="<?php echo $p['post_id']; ?>" class="like__total">
                                <?php 
                                    $likes = new Post($db);
                                    $total = $likes->countLikes($p['post_id']);
                                    echo $total;
                                ?>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="main__posts__item__user">
                    <?php if(!empty($p['avatar'])): ?>
                        <img src="<?php echo $p['avatar'] ?>" alt="<?php echo $p['username'] ?>">
                    <?php else: ?>
                        <img src="users/avatars/default-profile.png" alt="default avatar">
                    <?php endif; ?>
                    <a href="user.php?user=<?php echo $p['username'] ?>"><?php echo $p['fullname'] ?></a>
                </div>
            </li>
        <?php endforeach; ?>
        </ul>
        <?php if(empty($result)): ?>
            <div class="empty__state">
                <img class="empty__state__image" src="assets/vectors/empty-state.svg" alt="nu images are found">
                <p class="empty__state__heading">No Images Found</p>
                <p class="empty__state__text">Maybe you should make some friends or search something else</p>
            </div>
        <?php endif; ?>
    </main>
   
    </div>
    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>