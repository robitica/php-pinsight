# Pinsight-Inspiration-Hunter

### Rules
#### Finished feature
Commit when feature is finished = 
Example: Feature1 finished

### Notation CSS
Use BEM!
Exampe:
Block: class=form
Item: class=form__button
Modifier: class=form__button--danger
### Steps to follow when you start working on a feature:

##### Step 1 -> Fetch origin and Pull 

![alt text](https://i.imgur.com/lodTFNl.png "Fetch origin and Pull")
![alt text](https://i.imgur.com/QrvNv6d.png "Fetch origin and Pull")

##### Step 2 -> Create new branch

![alt text](https://i.imgur.com/DkSHFxX.png "Fetch origin and Pull")

##### Step 3 -> Start working on your feature and commit often

![alt text](https://i.imgur.com/RSWEEdd.png "Fetch origin and Pull")

##### Step 4 -> When the feature is ready and you are sure that everything is working, merge your feature branch with the master branch:

1. Change from you feature branch to master branch

   ![alt text](https://i.imgur.com/RSWEEdd.png "Fetch origin and Pull")

2. Branch > Marge into current branch

   ![alt text](https://i.imgur.com/b6sGRz2.png "Fetch origin and Pull")

3. Merge into master     

   ![alt text](https://i.imgur.com/DnnP2UR.png "Fetch origin and Pull")

##### Step 5 -> Push to origin

![alt text](https://i.imgur.com/xjSNHQk.png "Fetch origin and Pull")


