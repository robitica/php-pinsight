<?php
# color-extractor
require_once(dirname(__FILE__) . '/vendor/autoload.php');

use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

session_start();

spl_autoload_register(function($class) {
    include_once("classes/" . $class . ".class.php");
});

# authenticate user
User::authenticate();
   
# create database connection
$db = Db::getInstance();
 
# get user information
$actualUser = new User($db);
$userResults = $actualUser->getUserResults($_SESSION['username']);
$userId = $actualUser->getUserId($_SESSION['username']);

# if upload form is submitted
if(isset($_POST["upload"])){
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $city = $_POST['city'];
    $filter = $_POST['filter'];

    try {
        # UPLOAD IMAGE
        $post = new Post($db);
        $post->setDescription($_POST["description"]);
        $post->setLatitude($latitude);
        $post->setLongitude($longitude);
        $post->createImage();
        $post->createThumb(400, 300, 80);
        $post->createMarker(40, 40, 80);
        $post->saveTags();
        // make post private if private checkbox is selected
        if(isset($_POST['private'])) {
            $private = 1;
            $post->savePost($private, $userId, $city);
        } else {
            $private = 0;
            $post->savePost($private, $userId, $city);
        }
        $tags = $post->findTags();
        $post->bindPostWithTags($tags);

        # ColorExtractor
        $post->saveColors();

        # Image Filter
        $post->setFilter($filter);
        $post->saveFilter();
        
        header("Location: index.php");
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
 
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Upload - Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
</head>
<body>
    <?php include_once("includes/header.inc.php"); ?>
 
    <?php if(!empty($error)): ?>
        <div class="profile__change__msg profile__change__msg--error">
            <p><?php echo $error ?></p>
        </div>
    <?php endif; ?>
 
    <div class="container">
        <div class="container__header">
            <h1>Show us what you've been working on.</h1>
        </div>
 
        <main class="upload">
            <form class="upload__form" enctype="multipart/form-data" action="" method="post">
                <div class="post__preview">
                    <figure>
                        <img id="filePreview" style="display:none;">
                    </figure>
                </div>
                <div class="filters" style="visibility:hidden;">
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="none" checked>
                            <figure>
                                <img class="filter__preview">
                            </figure> 
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="_1977">
                            <figure class="_1977">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="aden">
                            <figure class="aden">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="brooklyn">
                            <figure class="brooklyn">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="earlybird">
                            <figure class="earlybird">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="nashville">
                            <figure class="nashville">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter">
                            <input type="radio" name="filter" value="moon">
                            <figure class="moon">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                    <div>
                        <label class="filter last">
                            <input type="radio" name="filter" value="walden">
                            <figure class="walden">
                                <img class="filter__preview">
                            </figure>
                        </label>
                    </div>
                </div>

                <fieldset class="upload__form__file">
                    <input type="file" name="fileToUpload" id="fileInput" accept="image/*">
                    <label class="fileInput" for="fileInput">
                        <figure>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path>
                            </svg>
                        </figure> 
                        <span>Click to upload image</span>
                    </label>
                </fieldset>
                <fieldset class="edit__field">
                    <label for="description"></label>
                    <textarea rows="10" cols="50" name="description" id="description" placeholder="Write a caption"></textarea>
                </fieldset>
                <fieldset>
                    <label for="private"></label>
                    <input type="checkbox" name="private" value="private"> Make post private (seen only by your friends)
                </fieldset>
                <fieldset class="hidden">
                    <label for="latitude">Latitude</label>
                    <input type="text" name="latitude" id="latitude" value="">
                </fieldset>
                <fieldset class="hidden">
                    <label for="longitude">Longitude</label>
                    <input type="text" name="longitude" id="longitude" value="">
                </fieldset>
                <fieldset class="hidden">
                    <label for="city">City</label>
                    <input type="text" name="city" id="city" value="">
                </fieldset>
                <input id="uploadBtn" class="preview__change__btn preview__change__btn--action preview__change__btn--upload preview__change__btn--margintop upload-result" type="submit" name="upload" value="Upload">
            </form>
        </main>
    </div>
 
    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
    <!-- SCRIPTS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9Ymb2DgI2E0NmU024kX_4RMyHHxptkVY&callback=initMap" type="text/javascript"></script>
    <script src="js/script.js"></script>
    
 
</body>
</html>