<?php
 
    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });
 
    // Check if logged in
    User::authenticate();
   
    // create database connection
    $db = Db::getInstance();
    // get user informations to use on posts (username, avatar, etc.)
    $user = new User($db);
    $userResults = $user->getUserResults($_SESSION['username']);
    // check the present page on page change  
    if(isset($_GET['sort'])) {
        $pageName = $_GET['sort'];
        $posts = new Post($db);
        if($pageName == 'friends') {
            // on friends page get 20 friends posts from database
            $id = $user->getUserId(strtolower($_SESSION['username']));
            // for load more without javascript
            if(isset($_GET['page'])) {
                $postsToSkip = ($_GET['page'] - 1) * 20;
                $postsToAdd = $_GET['page'] * 20;
                $result = $posts->getBatchFromFriends($id, $postsToSkip, $postsToAdd);
            } else {
                $result = $posts->getBatchFromFriends($id, 0, 20);
            }
        } elseif($pageName == 'followers') {
            // on followers page get 20 followers posts from database
            $id = $user->getUserId(strtolower($_SESSION['username']));
            // for load more without javascript
            if(isset($_GET['page'])) {
                $postsToSkip = ($_GET['page'] - 1) * 20;
                $postsToAdd = $_GET['page'] * 20;
                $result = $posts->getBatchFromFollowers($id, $postsToSkip, $postsToAdd);
            } else {
                $result = $posts->getBatchFromFollowers($id, 0, 20);
            }
        } elseif($pageName == 'nearby') {
            // on nearby page get 20 nearby posts from database
            $lat = $_GET['lat'];
            $lng = $_GET['long'];
            // for load more without javascript
            if(isset($_GET['page'])) {
                $postsToSkip = ($_GET['page'] - 1) * 20;
                $postsToAdd = $_GET['page'] * 20;
                $result = $posts->getBatchFromNearby($lat, $lng, $postsToSkip, $postsToAdd);
            } else {
                $result = $posts->getBatchFromNearby($lat, $lng, 0, 20);
            }
        }
    } else {
        // on home page get 20 posts from database
        $pageName = "index";
        $posts = new Post($db);
        // for load more without javascript
        if(isset($_GET['page'])) {
            $postsToSkip = ($_GET['page'] - 1) * 20;
            $postsToAdd = $_GET['page'] * 20;
            $result = $posts->getBatchFromAll($postsToSkip, $postsToAdd);
        } else {
            $result = $posts->getBatchFromAll(0, 20);
        }
    }
    //hide load more if no more posts exists
    if(count($result)<20) {
        $hideLoadMore = true;
    }
 
    //total reports
    $reports = new Post($db);
 
    //count comments
    $comment = new Comment($db);
 
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
</head>
<body>
<div id="wrapper">
    <!-- Navigatie -->
    <?php
        include_once("includes/header.inc.php");
    ?>
 
    <!-- Secondary menu -->
    <nav class="secondary__nav">
        <ul>
            <li class="<?php echo (!empty($pageName) && $pageName == 'followers') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php?sort=followers">Following</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'friends') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php?sort=friends">Friends</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'index') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php">All</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'nearby') ? 'secondary__nav__item nearby secondary__nav__item--selected' : 'secondary__nav__item nearby'; ?>"><a href="index.php?sort=nearby">Nearby</a></li>
            <li class="googlemaps secondary__nav__item"><a href="googlemaps.php">Map</a></li>
        </ul>
    </nav>
 
    <!-- Main container -->
    <main>
        <ul class="main__posts">
 
            <!-- Start list with posts -->
            <?php foreach ($result as $p): ?>
                <?php $totalReports = $reports->countReports($p[('post_id')]);  ?>
                <?php $totalComments = $comment->countComments($p['post_id']); ?>
               
                <?php if($totalReports<3): ?>
                <li class="main__posts__item">
                    <div class="main__posts__item__header">
                        <div class="main__posts__item__header__image">
                            <a class="main__posts__item__header__image__link" href="sight.php?id=<?php echo $p['post_id'] ?>">
                                <figure class= <?php echo $posts->showFilter($p['post_id']) ?>>
                                    <img src="<?php echo $p['thumb_image'] ?>" alt="post exemple">
                                </figure>
                            </a>
                            <a class="main__posts__item__header__image__hover" href="sight.php?id=<?php echo $p['post_id']; ?>">
                                <em class="timestamp"> <?php Post::timeAgo($p['post_time']); ?> </em>
                            </a>
                        </div>
                        <ul class="main__posts__item__header__info">
                            <li class="main__posts__item__header__info__item"><a href="sight.php?id=<?php echo $p['post_id']; ?>"><img src="assets/icons/comment.svg" alt="total comments"></a><p><?php echo $totalComments; ?></p></li>
                            <li class="main__posts__item__header__info__item">
                            <?php if($posts->isLiked($userResults['id'], $p['post_id'])== 0): ?>
                            <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post like">
                                <img src="assets/icons/like.svg" alt="total likes">
                            </a>
                            <?php else: ?>
                            <a href="#" id="<?php echo $p['post_id']; ?>" class="like__post">
                                <img src="assets/icons/liked.svg" alt="total likes">
                            </a>
                            <?php endif; ?>
                                <p id="<?php echo $p['post_id']; ?>" class="like__total">
                                    <?php
                                        $likes = new Post($db);
                                        $total = $likes->countLikes($p['post_id']);
                                        echo $total;
                                    ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <div class="main__posts__item__user">
                        <?php if(!empty($p['avatar'])): ?>
                            <img src="<?php echo $p['avatar'] ?>" alt="<?php echo $p['username'] ?>">
                        <?php else: ?>
                            <img src="users/avatars/default-profile.png" alt="default avatar">
                        <?php endif; ?>
                        <a href="user.php?user=<?php echo $p['username'] ?>"><?php echo $p['fullname'] ?></a>
                    </div>
                </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <?php if(empty($result)): ?>
            <div class="empty__state">
                <img class="empty__state__image" src="assets/vectors/empty-state.svg" alt="nu images are found">
                <p class="empty__state__heading">No Images Found</p>
                <p class="empty__state__text">Maybe you should make some friends or search something else</p>
            </div>
        <?php endif; ?>
        <?php if(!isset($hideLoadMore)): ?>
            <?php if(!isset($_GET['sort']) && !isset($_GET['page'])): ?>
                <div class="load__more">
                    <a id="<?php echo $pageName ?>" class="load__more__btn" href="index.php?page=2">Load more...</a>
                </div>
            <?php elseif(!isset($_GET['sort']) && isset($_GET['page'])): ?>
                <div class="load__more">
                    <a id="<?php echo $pageName ?>" class="load__more__btn" href="index.php?page=<?php echo $_GET['page']+1; ?>">Load more...</a>
                </div>
            <?php elseif(isset($_GET['sort']) && !isset($_GET['page'])): ?>
                <div class="load__more">
                    <a id="<?php echo $pageName ?>" class="load__more__btn" href="index.php?sort=<?php echo $_GET['sort']; ?>&page=2">Load more...</a>
                </div>
            <?php elseif(isset($_GET['sort']) && isset($_GET['page'])): ?>
                <div class="load__more">
                    <a id="<?php echo $pageName ?>" class="load__more__btn" href="index.php?sort=<?php echo $_GET['sort']; ?>&page=<?php echo $_GET['page']+1; ?>">Load more...</a>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </main>
   
    </div>

    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.1/moment.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9Ymb2DgI2E0NmU024kX_4RMyHHxptkVY&callback=initMap" type="text/javascript"></script>
    <script src="js/script.js"></script>
</body>
</html>