<?php
# color-extractor
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

    class Post {
        private $db;
        private $id;
        private $description;
        private $imagePath;
        private $thumbPath;
        //private $markerPath;
        private $tags;
        private $userId;
        private $colors;
        private $latitude;
        private $longitude;
        private $city;
        private $filter;
 
        public function __construct($db) {
            $this->db = $db;
        }

        ###                     ###
        ### GETTERS AND SETTERS ###
        ###                     ###
 
        # Get the value of id
        public function getId() {
            return $this->id;
        }
 
        # Get the value of description
        public function getDescription() {
            return $this->description;
        }
 
        # Set the value of description
        # @return  self
        public function setDescription($description) {
            if (empty($description)) {
                throw new Exception("Description cannot be empty.");
            }
            $this->description = $description;
            return $this;
        }

        # Get image path
        public function getImagePath() {
            return $this->imagePath;
        }

        # Get thumb path
        public function getThumbPath() {
            return $this->thumbPath;
        }

        # Get the value of colors
        public function getColors() {
            return $this->colors;
        }

        # Get the value of filter
        public function getFilter() {
            return $this->filter;
        }
       
        # Set the value of filter
        # @return  self
        public function setFilter($filter) {
            $this->filter = $filter;
            return $this;
        }
        
        # Get the value of userId
        public function getUserId() {
            return $this->userId;
        }

        # Set the value of userId
        # @return  self
        public function setUserId($userId) {
            $this->userId = $userId;
            return $this;
        }
 
        # Get the value of latitude
        public function getLatitude() {
                return $this->latitude;
        }

        # Set the value of latitude
        # @return  self
        public function setLatitude($latitude) {
                $this->latitude = $latitude;

                return $this;
        }

        # Get the value of longitude
        public function getLongitude() {
                return $this->longitude;
        }

        # Set the value of longitude
        # @return  self
        public function setLongitude($longitude) {
                $this->longitude = $longitude;

                return $this;
        }

        # Get the value of city
        public function getCity() {
            return $this->city;
        }

        # Set the value of city
        # @return  self
        public function setCity($city) {
                $this->city = $city;

                return $this;
        }

        # Get the value of markerPath
        /*public function getMarkerPath() {
            return $this->markerPath;
        }

        # Set the value of markerPath
        # @return  self
        public function setMarkerPath($markerPath) {
            $this->markerPath = $markerPath;
            return $this;
        }*/
 

        ###                     ###
        ###     FUNCTIONS       ###
        ###                     ###

        //////////////////////////////////////////////////////////
        ////////////// Store Images in Folder  ///////////////////
        //////////////////////////////////////////////////////////

        public function createImage() {
            if (!empty($_FILES['fileToUpload']['tmp_name'])) {
               if($_FILES['fileToUpload']['error'] == 0) {
                    $this->imagePath = "uploads/" . time() . basename($_FILES['fileToUpload']['name']);
                    $mime = pathinfo($this->imagePath,PATHINFO_EXTENSION);
                    $allowed = array('jpg', 'jpeg', 'gif', 'png');
                    if (in_array($mime, $allowed)) {
                        move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $this->imagePath);
                    } else {
                        throw new Exception("You cannot upload images of this filetype.");
                    }
               } else {
                    throw new Exception("An error occured while uploading your image.");
               }
            } else {
                throw new Exception("No image selected to upload.");
            }
        }

        public function createThumb($max_width, $max_height, $quality = 80) {
            # corect the image orientation mobile devices
            $this->correctImageOrientation();
            # capture original file information of uploaded image
            $imgsize = getimagesize($this->imagePath);
            $width = $imgsize[0];
            $height = $imgsize[1];
            $mime = $imgsize['mime'];
         
            # check extension type of uploaded image
            switch($mime){
                case 'image/png':
                    $image_create = "imagecreatefrompng";
                    $image = "imagepng";
                    $quality = 7;
                    break;
                case 'image/jpeg':
                    $image_create = "imagecreatefromjpeg";
                    $image = "imagejpeg";
                    $quality = 80;
                    break;
                default:
                    return false;
                    break;
            }

            $this->thumbPath = "uploads/thumbs/thumb-" . time() . basename($_FILES['fileToUpload']['name']);
             
            $src_img = $image_create($this->imagePath);
 
            # create temporary file (virtual)
            $dst_img = imagecreatetruecolor($max_width, $max_height);
 
            # create aspect ratio
            $width_new = $height * $max_width / $max_height;
            $height_new = $width * $max_height / $max_width;
           
            # if original image size is smaller than resizing limits
            if($width_new > $width) {
                # cut point by height
                $h_point = (($height - $height_new) / 2);
                # copy the original uploaded image at resized size
                imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
            } else {
                # cut point by width
                $w_point = (($width - $width_new) / 2);
                # copy the original uploaded image at resized size
                imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
            }
           
            # handle transparency by removing black (rgb: 0,0,0)
            $background = imagecolorallocate($dst_img , 0, 0, 0);
            imagecolortransparent($dst_img, $background);
            $image($dst_img, $this->thumbPath, $quality);
 
            # free memory
            if($dst_img)imagedestroy($dst_img);
            if($src_img)imagedestroy($src_img);
            
        }
        /*
        public function createMarker($max_width, $max_height, $quality = 80) {
            # capture original file information of uploaded image
            $imgsize = getimagesize($this->imagePath);
            $width = $imgsize[0];
            $height = $imgsize[1];
            $mime = $imgsize['mime'];
         
            # check extension type of uploaded image
            switch($mime){
                case 'image/png':
                    $image_create = "imagecreatefrompng";
                    $image = "imagepng";
                    $quality = 7;
                    break;
                case 'image/jpeg':
                    $image_create = "imagecreatefromjpeg";
                    $image = "imagejpeg";
                    $quality = 80;
                    break;
                default:
                    return false;
                    break;
            }

            $this->markerPath = "uploads/markers/marker-" . time() . basename($_FILES['fileToUpload']['name']);
             
            $src_img = $image_create($this->imagePath);
 
            # create temporary file (virtual)
            $dst_img = imagecreatetruecolor($max_width, $max_height);
 
            # create aspect ratio
            $width_new = $height * $max_width / $max_height;
            $height_new = $width * $max_height / $max_width;
           
            # if original image size is smaller than resizing limits
            if($width_new > $width) {
                # cut point by height
                $h_point = (($height - $height_new) / 2);
                # copy the original uploaded image at resized size
                imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
            } else {
                # cut point by width
                $w_point = (($width - $width_new) / 2);
                # copy the original uploaded image at resized size
                imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
            }
           
            # handle transparency by removing black (rgb: 0,0,0)
            $background = imagecolorallocate($dst_img , 0, 0, 0);
            imagecolortransparent($dst_img, $background);
            $image($dst_img, $this->markerPath, $quality);
 
            # free memory
            if($dst_img)imagedestroy($dst_img);
            if($src_img)imagedestroy($src_img);
            
        }*/

        private function correctImageOrientation() {
            if (function_exists('exif_read_data')) {
              $exif = exif_read_data($this->imagePath);
              if($exif && isset($exif['Orientation'])) {
                $orientation = $exif['Orientation'];
                if($orientation != 1){
                  $img = imagecreatefromjpeg($this->imagePath);
                  $deg = 0;
                  switch ($orientation) {
                    case 3:
                      $deg = 180;
                      break;
                    case 6:
                      $deg = 270;
                      break;
                    case 8:
                      $deg = 90;
                      break;
                  }
                  if ($deg) {
                    $img = imagerotate($img, $deg, 0);       
                  }
                  // then rewrite the rotated image back to the disk as $filename
                  imagejpeg($img, $this->imagePath, 95);
                } // if there is some rotation necessary
              } // if have the exif orientation info
            } // if function exists     
          }

 
        //////////////////////////////////////////////////////////
        /////////////// Save and Fetch Posts /////////////////////
        //////////////////////////////////////////////////////////
 
        public function savePost($private, $userId, $city) {
            // save to db
            $statement = $this->db->prepare("insert into posts(private, image, thumb_image, description, user_id, post_time, longitude, latitude, city) VALUES (:private, :image, :thumb_image, :description, :user_id, :post_time, :longitude, :latitude, :city)");
            $statement->bindValue(":private", $private);
            $statement->bindValue(":image", $this->imagePath);
            $statement->bindValue(":thumb_image", $this->thumbPath);
            //$statement->bindValue(":marker_image", $this->markerPath);
            $statement->bindValue(":description", $this->description);
            $statement->bindValue(":user_id", $userId);
            $statement->bindValue(":post_time", date("Y-m-d H:i:sa"));
            $statement->bindValue(":longitude", $this->getLongitude());
            $statement->bindValue(":latitude", $this->getLatitude());
            $statement->bindValue(":city", $city);
            $statement->execute();
            // update id
            $this->id = $this->db->lastInsertId();
        }

        public function getOne($post_id) {
            $statement = $this->db->prepare("SELECT * FROM posts INNER JOIN users ON posts.user_id = users.id WHERE posts.id = :post_id");
            $statement->bindParam(":post_id", $post_id);
            $statement->execute();
            return $statement->fetch(PDO::FETCH_ASSOC);
        }

        public function getAllPosts() {
            $statement = $this->db->prepare("SELECT * FROM posts ORDER BY id");
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
       
        public function getBatchFromAll($postsOnPage, $postsToBeAdded) {
            $statement = $this->db->prepare("SELECT *, posts.id as post_id FROM posts INNER JOIN users ON posts.user_id = users.id WHERE posts.private != 1 ORDER BY post_id DESC LIMIT ".$postsOnPage.", ".$postsToBeAdded."");
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }
 
        public function getBatchFromFriends($id, $postsOnPage, $postsToBeAdded) {
            $statement = $this->db->prepare("SELECT *, posts.id as post_id FROM posts INNER JOIN users ON posts.user_id = users.id INNER JOIN relationship ON users.id = relationship.user_one_id OR users.id = relationship.user_two_id WHERE relationship.status = 1 AND users.username != :username AND (relationship.user_one_id = :id OR relationship.user_two_id = :id) ORDER BY post_id ASC LIMIT $postsOnPage, $postsToBeAdded");
            $statement->bindValue(":username", strtolower($_SESSION['username']));
            $statement->bindParam(":id", $id);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getBatchFromFollowers($id, $postsOnPage, $postsToBeAdded) {
            $statement = $this->db->prepare("SELECT *, posts.id as post_id, tags.id AS the_tag_id, follow_tags.user AS follower_taggy FROM posts_tags 
            LEFT JOIN posts ON posts_tags.post_id = posts.id
            LEFT JOIN users ON posts.user_id = users.id 
            LEFT JOIN follow ON users.id = follow.sender OR users.id = follow.receiver 
            LEFT JOIN follow_tags ON follow_tags.user = users.id
            LEFT JOIN tags ON posts_tags.tag_id = tags.id
            WHERE follow_tags.user = :id AND follow_tags.tag = posts_tags.tag_id
            OR follow.sender = :id AND users.username != :username AND posts.private != 1
            group by posts.id
            ORDER BY posts_tags.tag_id ASC 
            LIMIT ".$postsOnPage.", ".$postsToBeAdded."");
            $statement->bindValue(":username", strtolower($_SESSION['username']));
            $statement->bindParam(":id", $id);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getBatchFromNearby($lat, $lng, $postsOnPage, $postsToBeAdded) {
            $statement = $this->db->prepare(
            "SELECT *, posts.id as post_id, 111.045 * DEGREES(ACOS(COS(RADIANS(:lat)) * COS(RADIANS(latitude))
             * COS(RADIANS(longitude) - RADIANS(:lng))
             + SIN(RADIANS(:lat))
             * SIN(RADIANS(latitude))))
             AS distance_in_km
            FROM posts
            INNER JOIN users ON posts.user_id = users.id
            WHERE posts.private != 1
            ORDER BY distance_in_km ASC
            LIMIT ".$postsOnPage.",".$postsToBeAdded.";");
            $statement->bindValue(":lat", $lat);
            $statement->bindValue(":lng", $lng);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getAllFromUser($username, $relationshipStatus) {
            $statement = $this->db->prepare("SELECT *, posts.id as post_id FROM posts INNER JOIN users ON posts.user_id = users.id WHERE users.username = :username AND posts.private != :relationshipStatus ORDER BY post_id DESC");
            $statement->bindParam(":username", $username);
            $statement->bindParam(":relationshipStatus", $relationshipStatus);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        //////////////////////////////////////////////////////////
        /////////////// Update and Delete Post ///////////////////
        //////////////////////////////////////////////////////////
 
        public function updatePost($post_id) {
            // update posts set description = description waar post_id = post
            $statement = $this->db->prepare("update posts set description = :description where id = :post");
            $statement->bindValue(":description", $this->getDescription());
            $statement->bindValue(":post", $post_id);
            $statement->execute();
        }
 
        public function deletePost($postId) {
            $statement = $this->db->prepare("DELETE FROM posts WHERE id = :post");
            $statement->bindValue(":post", $postId);
            $statement->execute();
        }

        //////////////////////////////////////////////////////////
        ///////////////////// Location  //////////////////////////
        //////////////////////////////////////////////////////////

        public function get_api ($lat, $long) {
            $get_API = "http://maps.googleapis.com/maps/api/geocode/json?latlng=";
            $get_API .= round($lat,2).",";
            $get_API .= round($long,2);         
        
            $jsonfile = file_get_contents($get_API.'&sensor=false');
            $jsonarray = json_decode($jsonfile);     
            if (isset($jsonarray->results[1]->address_components[0]->long_name)) {
                return($jsonarray->results[1]->address_components[0]->long_name);
            }
            else {
                return('Unknown');
            }
        }
        
        public function getCityName($postId){
            $statement = $this->db->prepare("SELECT city FROM posts WHERE id = :post_id");
            $statement->bindParam(":post_id", $postId);
            $statement->execute();
            return $statement->fetch(PDO::FETCH_ASSOC);
        }

        //////////////////////////////////////////////////////////
        /////////////////////// Time  ////////////////////////////
        //////////////////////////////////////////////////////////

        public static function timeAgo($date){
            if(time("Y-m-d H:i:sa") - strtotime($date) < 60){
                echo 'Just now';
            } elseif( time("Y-m-d H:i:sa") - strtotime($date) < 3600){
                echo floor((time("Y-m-d H:i:sa") - strtotime($date))/ 60) . ' mins ago';
            } elseif( time("Y-m-d H:i:sa") - strtotime($date) < 86400){
                echo floor((time("Y-m-d H:i:sa") - strtotime($date))/ 3600) . ' hrs ago';
            } else{
                echo floor((time("Y-m-d H:i:sa") - strtotime($date))/ 86400) . ' days ago';
            }
        }

        //////////////////////////////////////////////////////////
        ////////////////// Color Extractor ///////////////////////
        //////////////////////////////////////////////////////////

        public function saveColors() {
            // extract from image
            $palette = Palette::fromFilename($this->thumbPath);
            $extractor = new ColorExtractor($palette);
            $colors = $extractor->extract(5);
            // and save to db
            foreach($colors as $color) {
                $statement = $this->db->prepare("INSERT into posts_colors (post_id, hex) VALUES (:postId, :hex)");
                $statement->bindValue(":postId", $this->id);
                $statement->bindValue(":hex", Color::fromIntToHex($color));
                $statement->execute();
            }
        }

        public function fetchColors($postId) {
            $statement = $this->db->prepare("SELECT * FROM posts_colors WHERE post_id = :postId");
            $statement->bindParam(":postId", $postId);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        public function searchPostsOnColor($color) {
            $statement = $this->db->prepare("SELECT * FROM posts_colors INNER JOIN posts ON posts.id = posts_colors.post_id INNER JOIN users ON users.id = posts.user_id WHERE posts_colors.hex = :color AND posts.private != 1");
            $statement->bindParam(":color", $color);
            $statement->execute();
            return $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        //////////////////////////////////////////////////////////
        ///////////////// Instagram Filters //////////////////////
        //////////////////////////////////////////////////////////
     
        public function saveFilter() {
            $statement = $this->db->prepare("INSERT into posts_filter (post_id, filtername) VALUES (:postId, :filter)");
            $statement->bindValue(":postId", $this->id);
            $statement->bindValue(":filter", $this->filter);
            $statement->execute();
        }

        public function showFilter($postId) {
            //$statement = $this->db->prepare("SELECT * FROM posts_filter WHERE post_id = :postId");
            $statement = $this->db->prepare("SELECT * FROM posts_filter INNER JOIN posts ON posts_filter.post_id = posts.id WHERE posts.id = :postId");
            $statement->bindParam(":postId", $postId);
            $statement->execute();
            $result = '';
            $filters = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($filters as $f) {
               $result = $f['filtername'];
            } 
            return $result;
        }

        ////////////////////////////////////////////////////
        ////////////////// Search Posts ////////////////////
        ////////////////////////////////////////////////////
 
        public function search($searchParams) {
            $search = join(" ", $searchParams);
            if(count($searchParams) > 1) {
                $tags = join("','", $searchParams);
            } else {
                $tags = $searchParams[0];
            }
            $statement = $this->db->prepare("SELECT *, tags.id AS the_tag_id, posts.id AS post_id FROM posts
            LEFT JOIN posts_tags ON posts.id = posts_tags.post_id  
            LEFT JOIN tags ON posts_tags.tag_id = tags.id
            LEFT JOIN users ON posts.user_id = users.id 
            WHERE tag_name IN ('$tags') AND posts.private != 1
            OR tag_name IN ('$search') AND posts.private != 1
            OR description LIKE '%$search%' AND posts.private != 1
            OR posts.city LIKE '$search' AND posts.private != 1
            group by posts.id
            order by posts.id");
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        private function separateTagsFromDescription() {
            $keywords = [];
            /* Match hashtags */
            preg_match_all('/#(\w+)/', $this->getDescription(), $matches);
             /* Add all matches to array */
            foreach ($matches[1] as $match) {
                $keywords[] = $match;
            }
            return $keywords;
        }

        public static function returnLinkFromTag($description) {
            preg_match_all('/#(\w+)/', $description, $matches);
             foreach ($matches[1] as $match) {
               $description = str_replace("#$match", "<a href='search.php?search=$match'>#$match</a>", "$description");
             }
            
            return $description;
        }
 
        public function bindPostWithTags($tags) {
            foreach ($tags as $t) {
                $statement = $this->db->prepare("insert into posts_tags (post_id, tag_id) VALUES (:post, :tag)");
                $statement->bindValue(":post", $this->id);
                $statement->bindValue(":tag", $t);
                $statement->execute();
            }
        }
 
        public function editPostTags($tags, $postId) {
            foreach ($tags as $t) {
                $statement = $this->db->prepare("insert into posts_tags (post_id, tag_id) VALUES (:post, :tag)");
                $statement->bindValue(":post", $postId);
                $statement->bindValue(":tag", $t);
                $statement->execute();
            }
        }

        public function findTags() {
            $tags = $this->separateTagsFromDescription();
            $selectedTags = [];
            foreach($tags as $key => $t) {
                $statement = $this->db->prepare("SELECT id FROM tags WHERE tag_name = :tag");
                $statement->bindValue(":tag", $t);
                $statement->execute();
                $tagId = $statement->fetch(PDO::FETCH_ASSOC);
                $selectedTags[$key] = $tagId['id'];
            }
            return $selectedTags;
        }

        private function checkIfTagExists() {
            $tagsToBeAdded = [];
            $totalTags = $this->separateTagsFromDescription();
            foreach($totalTags as $key => $t) {
                $statement = $this->db->prepare("SELECT * FROM tags WHERE tag_name = :tag");
                $statement->bindValue(":tag", $t);
                $statement->execute();
                $result = $statement->fetch(PDO::FETCH_ASSOC);
                if(!$result) {
                    $tagsToBeAdded[$key] = $t;
                }
            }
            return $tagsToBeAdded;
        }
 
        public function saveTags() {
            $tagsToBeAdded = $this->checkIfTagExists();
            foreach($tagsToBeAdded as $t) {
                $statement = $this->db->prepare("insert into tags (tag_name) VALUES (:tag)");
                $statement->bindValue(":tag", $t);
                $statement->execute();
            }
        }

        public function getAllTags($postId) {
            $statement = $this->db->prepare("SELECT * FROM posts_tags INNER JOIN posts ON posts_tags.post_id = posts.id INNER JOIN tags ON posts_tags.tag_id = tags.id WHERE posts.id = :post");
            $statement->bindValue(":post", $postId);
            $statement->execute();
            $result = [];
            $tags = $statement->fetchAll(PDO::FETCH_ASSOC);
            foreach($tags as $t) {
                $result[] = $t['tag_name'];
            }
            return $result;
        }

        public function deleteTags($postId) {
            $statement = $this->db->prepare("DELETE FROM posts_tags WHERE post_id = :post");
            $statement->bindValue(":post", $postId);
            $statement->execute();
        }

        public function checkIfTag($search){
            $statement = $this->db->prepare("select * from tags WHERE tag_name = :tag");
            $statement->bindValue(":tag", $search);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        public function startTagFollow($userId, $tagId){
            //insert into follow where user_id = you and follow_id is = follower
            $statement = $this->db->prepare("INSERT INTO `follow_tags` (`user`, `tag`) VALUES (:user, :tag) ");
            $statement->bindValue(":user", $userId);
            $statement->bindValue(":tag", $tagId);
            $statement->execute();
        }

        //this is our unfollow method
        public function unFollowTag($userId, $tagId) {
            //delete user_id and follow_id from follow 
            $statement = $this->db->prepare("DELETE FROM `follow_tags` WHERE `user` = :user and `tag` = :tag");
            $statement->bindValue(":user", $userId);
            $statement->bindValue(":tag", $tagId);
            $statement->execute();
        }

        //check if the user tagfollowed
        public function checkIfTagFollowed($userId, $tagId) {
            //get true if follow or false if not followed
            $statement = $this->db->prepare("select * from follow_tags WHERE user = :user AND tag = :tag");
            $statement->bindValue(":user", $userId);
            $statement->bindValue(":tag", $tagId);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if(!$result) {
                return "<a href='#' id='".$tagId."' class='follow tag_follow_btn'>Follow</a>";
            } else {
                return "<a href='#' id='".$tagId."' class='following tag_follow_btn'>Following</a>";
            }
        }

        ////////////////////////////////////////////////////
        //////////////////// Likes /////////////////////////
        ////////////////////////////////////////////////////

        public function likePost($user_id, $post_id) {
            //insert into posts_likes where user_id = you and post_id = post you like
            $statement = $this->db->prepare("INSERT INTO posts_likes (post_id, user_id) VALUES (:post, :user)");
            $statement->bindValue(":post", $post_id);
            $statement->bindValue(":user", $user_id);
            $statement->execute();
        }

        public function disLikePost($user_id, $post_id) {
            //delete user_id and post_id from posts_likes??
            $statement = $this->db->prepare("DELETE FROM posts_likes WHERE post_id = :post and user_id = :user");
            $statement->bindValue(":post", $post_id);
            $statement->bindValue(":user", $user_id);
            $statement->execute();
        }

        public function countLikes($post_id) {
            //tel de rijen waarvan post_id = van de post die je liked
            $statement = $this->db->prepare("SELECT * FROM posts_likes WHERE post_id = :post");
            $statement->bindValue(":post", $post_id);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            return count($result);
        }

        public function isLiked($user_id, $post_id) {
            $statement = $this->db->prepare("SELECT * FROM posts_likes WHERE user_id = :user AND post_id = :post");
            $statement->bindValue(":user", $user_id);
            $statement->bindValue(":post", $post_id);
            $statement->execute();
            $result = $statement->rowCount();
            return $result;
        }

        ////////////////////////////////////////////////////
        //////////////////// Reports ///////////////////////
        ////////////////////////////////////////////////////

        public function reportPost($user_id, $post_id) {
            //insert into posts_reports where user_id = you and post_id = post you report
            $statement = $this->db->prepare("INSERT INTO posts_reports (post_id, user_id) VALUES (:post, :user) ");
            $statement->bindValue(":post", $post_id);
            $statement->bindValue(":user", $user_id);
            $statement->execute();
        }

        public function unReport($user_id, $post_id) {
            //delete user_id and post_id from posts_likes??
            $statement = $this->db->prepare("DELETE FROM posts_reports WHERE post_id = :post and user_id = :user");
            $statement->bindValue(":post", $post_id);
            $statement->bindValue(":user", $user_id);
            $statement->execute();
        }

        public function isReported($user_id, $post_id) {
            //check if post is reported by current user
            $statement = $this->db->prepare("SELECT * FROM posts_reports WHERE user_id = :user AND post_id = :post");
            $statement->bindValue(":user", $user_id);
            $statement->bindValue(":post", $post_id);
            $statement->execute();
            $result = $statement->rowCount();
            return $result;
        }

        public function countReports($post_id) {
            //tel de rijen waarvan post_id = van de post die je liked
            $statement = $this->db->prepare("SELECT * FROM posts_reports WHERE post_id = :post");
            $statement->bindValue(":post", $post_id);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            return count($result);
        }

        //////////////////////////////////////////////////////////
        ////////////// Related posts from user ///////////////////
        //////////////////////////////////////////////////////////

        public function getRelatedPosts($userId, $postId){
            //neem laatste posts waarvan user is gelijk aan posting user 
            $statement = $this->db->prepare
            ("SELECT * FROM posts WHERE user_id = $userId AND id != $postId
            ORDER BY id DESC LIMIT 3");
            $statement->execute();
            $result = $statement->fetchAll();
            return $result;
        }

        //////////////////////////////////////////////////////////
        ////////////// Filter map markers by hashtags ////////////
        //////////////////////////////////////////////////////////
        public function filterByTags($filter) {
            $statement = $this->db->prepare("SELECT * FROM posts
                INNER JOIN posts_tags ON posts.id = posts_tags.post_id
                INNER JOIN tags ON posts_tags.tag_id = tags.id
                WHERE tags.tag_name = :filterValue");
            $statement->bindParam(":filterValue", $filter);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        

    }
 
?>