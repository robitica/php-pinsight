<?php

    class Comment{
        private $db;
        private $userId;
        private $commentTxt;
        private $date_posted;

        public function __construct($db) {
            $this->db = $db;
        }

        ////////////////////////////////////////////////////
        ////////////// getters setters /////////////////////
        ////////////////////////////////////////////////////

                /**
         * Get the value of userId
         */ 
        public function getUserId()
        {
                return $this->userId;
        }

        /**
         * Set the value of userId
         *
         * @return  self
         */ 
        public function setUserId($userId)
        {
                $this->userId = $userId;

                return $this;
        }

                /**
         * Get the value of commentTxt
         */ 
        public function getCommentTxt()
        {
                return $this->commentTxt;
        }

        /**
         * Set the value of commentTxt
         *
         * @return  self
         */ 
        
        public function setCommentTxt($commentTxt) {
            if(empty($commentTxt)) {
                throw new Exception("The comment can not be empty");
            }
            $this->commentTxt = $commentTxt;
            return $this;
        }
                /**
         * Get the value of date_posted
         */ 
        public function getDate_posted()
        {
                return $this->date_posted;
        }

        /**
         * Set the value of date_posted
         *
         * @return  self
         */ 
        public function setDate_posted($date_posted)
        {
                $this->date_posted = $date_posted;

                return $this;
        }

        ////////////////////////////////////////////////////
        ////////////////// functions ///////////////////////
        ////////////////////////////////////////////////////

        public function saveComment($postId) {
            $statement = $this->db->prepare("INSERT INTO comments (text, user_id, post_id, date_posted) VALUES (:comment, :user_id, :post_id, :date_posted) ");
            $statement->bindValue(":comment", $this->getcommentTxt());
            $statement->bindValue(":user_id", $this->userId);
            $statement->bindValue(":post_id", $postId);
            $statement->bindValue(":date_posted", $this->date_posted);
            $statement->execute();
        }

        public function getAllComments($postId) {
            $statement = $this->db->prepare
            ("SELECT * 
            from comments 
            INNER join users 
            ON comments.user_id = users.id
            WHERE post_id = $postId
            ORDER BY comments.id 
            DESC");
            $statement->execute();
            $result = $statement->fetchAll();
            return $result;
        }

        public function countComments($post_id) {
            //tel de rijen waarvan post_id = van de post die je liked
            $statement = $this->db->prepare("SELECT * FROM comments WHERE post_id = :post");
            $statement->bindValue(":post", $post_id);
            $statement->execute();
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            return count($result);
        }
    }

?>