<?php

    class Relationship {

        public $userOne;
        public $userTwo;
        
        /**
         * Determines the status of the relationship
         * 
         * 0 - Pending
         * 1 - Accepted
         * 2 - Declined
         * 3 - Blocked
         * 
         * By default the status is set to 0
         */
        public $status = 0;
        
        /**
         * This is the user who made the most recent status field update
         */
        public $actionUserId;
        private $db;

        public function __construct($db) {
            $this->db = $db;
        }
            
        public function getUserOne() {
            return $this->userOne;
        }
        
        public function setUserOne($userOne) {
            $this->userOne = $userOne;
        }
        
        public function getUserTwo() {
            return $this->userTwo;
        }
        
        public function setUserTwo($userTwo) {
            $this->userTwo = $userTwo;
        }
        
        public function getStatus() {
            return $this->status;
        }
        
        public function setStatus($status) {
            $this->status = $status;
        }
        
        public function getActionUserId() {
            return $this->actionUserId;
        }
        
        public function setActionUserId($actionUserId) {
            $this->actionUserId = $actionUserId;
        }

        public function addFriendRequest() {
            $user_one = $this->getUserOne();
            $action_user_id = $user_one;
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
                $temp = $user_one;
                $user_one = $user_two;
                $user_two = $temp;
            }
            
            $statement = $this->db->prepare(
              'INSERT INTO `relationship` '
            . '(`user_one_id`, `user_two_id`, `status`, `action_user_id`) '
            . 'VALUES '
            . '(' . $user_one . ', '. $user_two .', 0, '. $action_user_id .')');
            $statement->execute();
        }

        public function acceptFriendRequest() {
            $user_one = $this->getUserOne();
            $action_user_id = $user_one;
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
                $temp = $user_one;
                $user_one = $user_two;
                $user_two = $temp;
            }
            
            $statement = $this->db->prepare(
              'UPDATE `relationship` '
            . 'SET `status` = 1, `action_user_id` = '. $action_user_id 
            .' WHERE `user_one_id` = '. $user_one 
            .' AND `user_two_id` = ' . $user_two);
            $statement->execute();
        }

        public function declineFriendRequest(User $user) {
            $user_one = $this->getUserOne();
            $action_user_id = $user_one;
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
                $temp = $user_one;
                $user_one = $user_two;
                $user_two = $temp;
            }
            
            $statement = $this->db->prepare(
              'UPDATE `relationship` '
            . 'SET `status` = 2, `action_user_id` = '. $action_user_id 
            .' WHERE `user_one_id` = '. $user_one 
            .' AND `user_two_id` = ' . $user_two);
            $statement->execute();
        }

        public function unfriend() {
            $user_one = $this->getUserOne();
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
            $temp = $user_one;
            $user_one = $user_two;
            $user_two = $temp;
            }
            
            $statement = $this->db->prepare( 
            'DELETE FROM `relationship` ' .
            'WHERE `user_one_id` = ' . $user_one . 
            ' AND `user_two_id` = ' . $user_two .
            ' AND `status` = 1');
            $statement->execute();
        }

        public function getRelationshipStatus() {
            $user_one = $this->getUserOne();
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
                $temp = $user_one;
                $user_one = $user_two;
                $user_two = $temp;
            }
            
            $statement = $this->db->prepare(
            'SELECT * FROM `relationship` ' .
            'WHERE `user_one_id` = ' . $user_one . 
            ' AND `user_two_id` = ' . $user_two);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
        
            if(!$result) {
                return false;
            } else {
                return $result['status'];
            }
          }

          private function getRelationshipActionUserId() {
            $user_one = $this->getUserOne();
            $user_two = $this->getUserTwo();
            
            if ($user_one > $user_two) {
                $temp = $user_one;
                $user_one = $user_two;
                $user_two = $temp;
            }
            
            $statement = $this->db->prepare(
            'SELECT * FROM `relationship` ' .
            'WHERE `user_one_id` = ' . $user_one . 
            ' AND `user_two_id` = ' . $user_two);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
        
            if(!$result) {
                return false;
            } else {
                return $result['action_user_id'];
            }
          }

          public function prepareFriendButton() {
              if($this->getRelationshipStatus() === false) {
                $btnClass = " friend__btn friend__btn--add preview__change__btn preview__change__btn--action";
                $btnText = "Add Friend";
              } elseif (($this->getRelationshipStatus() == 0) && ($this->getRelationshipActionUserId() == $this->getUserOne())) {
                $btnClass = " friend__btn friend__btn--pending preview__change__btn preview__change__btn--action";
                $btnText = "Pending...";
              } elseif (($this->getRelationshipStatus() == 0) && ($this->getRelationshipActionUserId() !== $this->getUserOne())) {
                $btnClass = "friend__btn friend__btn--accept preview__change__btn preview__change__btn--action";
                $btnText = "Accept Friend";
              } else {
                $btnClass = "friend__btn friend__btn--unfriend preview__change__btn preview__change__btn--danger";
                $btnText = "Unfriend";
              }

              return "<a href='#' id='".$_GET['user']."' class='".$btnClass."'>".$btnText."</a>";
          }

    }

?>