<?php

    class Follow extends User {

        
        ////////////////////////////////////////////////////
        //////////////////// User Follow ///////////////////
        ////////////////////////////////////////////////////

        public function startFollow($user_id, $follower_id) {
            //insert into follow where user_id = you and follow_id is = follower
            $statement = $this->db->prepare("INSERT INTO `follow` (`sender`, `receiver`) VALUES (:user, :follower) ");
            $statement->bindValue(":user", $user_id);
            $statement->bindValue(":follower", $follower_id);
            $statement->execute();
        }
        
        //this is our unfollow method
        public function unFollow($user_id, $follower_id) {
            //delete user_id and follow_id from follow 
            $statement = $this->db->prepare("DELETE FROM `follow` WHERE `sender` = :user and `receiver` = :follower");
            $statement->bindValue(":user", $user_id);
            $statement->bindValue(":follower", $follower_id);
            $statement->execute();
        }

        //check if the user is following
        public function checkIfFollowed($user_id, $follower_id) {
            //get true if follow or false if not followed
            $statement = $this->db->prepare("select * from follow WHERE sender = :user AND receiver = :follower");
            $statement->bindValue(":user", $user_id);
            $statement->bindValue(":follower", $follower_id);
            $statement->execute();
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if(!$result) {
                return "<a href='#' id='".$_GET['user']."' class='follow follow__btn preview__change__btn preview__change__btn--action'>Follow</a>";
            } else {
                return "<a href='#' id='".$_GET['user']."' class='following follow__btn preview__change__btn preview__change__btn--danger'>Following</a>";
            }
        }

        ////////////////////////////////////////////////////
        ///////////////////// Tag Follow ///////////////////
        ////////////////////////////////////////////////////

    }

?>