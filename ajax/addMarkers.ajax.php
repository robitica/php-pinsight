<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    // get database connection
    $db = Db::getInstance();

    $posts = new Post($db);
    $locations = [];
   
    if(!empty($_POST)) {
        
        // data returned from ajax request
        $filter = $_POST['filter'];
        $locations = $posts->filterByTags($filter);
        
    } else {
        $locations = $posts->getAllPosts();
    };

    $feedback = [
        "status" => "success",
        "locations" => $locations
    ];
    
    //return http JSON header
    header('Content-Type:json/application');
    echo json_encode($feedback);

?>