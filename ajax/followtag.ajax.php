<?php 
    session_start();
    
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    $tagId = $_POST['tag'];
    $followTagStatus = $_POST['followStatus'];

    // create database connection
    $db = Db::getInstance();

    $user = new User($db);
    $userId = $user->getUserId($_SESSION['username']);

    $tag = new Post($db);

    //if user submit follow 
    if($followTagStatus == "Follow") {
        $tag->startTagFollow($userId, $tagId);
        $followTagStatus = "following";
    }

    //if user submit unfollow 
    if($followTagStatus == "Following") {
        $tag->unFollowTag($userId, $tagId);
        $followTagStatus = "follow";
    }

    $response = [
        "status"               =>      "success",
        "tagId"                =>      $tagId,
        "followTagStatus"      =>      $followTagStatus
    ];

    header('Content-Type: application/json');
    echo json_encode($response);
?>