<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });
   
    if(!empty($_POST)) {
        // get data from ajax request
        $page = $_POST['page'];
        $newPostsOnPage = $_POST['newPostsOnPage'];
        $newPostsToBeAdded = $_POST['newPostsToBeAdded'];
        // create database connection
        $db = Db::getInstance();
        // get user id
        $user = new User($db);
        $id = $user->getUserId(strtolower($_SESSION['username']));
        // get posts
        $posts = new Post($db);
        // get comments
        $comments = new Comment($db);
        // check the present page on page change
        if($page == "index") {
            // on home page get next 20 posts from database
            $newPosts = $posts->getBatchFromAll($newPostsOnPage, $newPostsToBeAdded);
        } elseif($page == "friends") {
            // on friends page get next 20 friends posts from database
            $newPosts = $posts->getBatchFromFriends($id, $newPostsOnPage, $newPostsToBeAdded);
        } elseif($page == "followers") {
            // on followers page get next 20 followers posts from database
            $newPosts = $posts->getBatchFromFollowers($id, $newPostsOnPage, $newPostsToBeAdded);
        } elseif($page == "nearby") {
            // on nearby page get next 20 nearby posts from database
            $lat = 50.9380287;
            $lng = 4.4397977;
            $newPosts = $posts->getBatchFromNearby($lat, $lng, $newPostsOnPage, $newPostsToBeAdded);
        } else {
            header("Location: index.php");
        }

        //hide load more if no more posts exists
        $hideLoadMore;
        if(count($newPosts)<20) {
            $hideLoadMore = true;
        }
 
        $likes = [];
        $totalLikes = [];
        $totalComments = [];
        foreach($newPosts as $key => $np) {
            $likes[$key] = $posts->isLiked($id, $np['post_id']);
            $totalLikes[$key] = $posts->countLikes($np['post_id']);
            $totalComments[$key] = $comments->countComments($np['post_id']);
        }
 
        $response = [
            "status"        =>  "success",
            "posts"         =>  $newPosts,
            "likes"         =>  $likes,
            "totalLikes"    =>  $totalLikes,
            "totalComments" =>  $totalComments,
            "hideLoadMore"  =>  $hideLoadMore
        ];
   
        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>