<?php 
    session_start();
    
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    $followerUsername = $_POST['userPage'];
    $followStatus = $_POST['followStatus'];

    // create database connection
    $db = Db::getInstance();

    $user = new Follow($db);
    $senderId = $user->getUserId($_SESSION['username']);
    $receiverId = $user->getUserId($followerUsername);

    //if user submit follow 
    if($followStatus == "Follow") {
        $user->startFollow($senderId, $receiverId);
        $followStatus = "following";
    }

    //if user submit unfollow 
    if($followStatus == "Following") {
        $user->unFollow($senderId, $receiverId);
        $followStatus = "follow";
    }

    $response = [
        "status"            =>      "success",
        "followerUsername"  =>      $followerUsername,
        "followStatus"      =>      $followStatus
    ];

    header('Content-Type: application/json');
    echo json_encode($response);
?>