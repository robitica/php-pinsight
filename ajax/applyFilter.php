<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });
    // get database connection
    $db = Db::getInstance();

    $filter = new Post($db);
    $filterValue = $_POST['filtername'];
    try {
        $filter->setFilter($filterValue);

        $feedback = [
            "status" => "success",
            "filter" => $filterValue
        ];
        
    } catch (Exception $e) {
        $feedback['status'] = "error";
    }
    
    //return http JSON header
    header('Content-Type:json/application');
    echo json_encode($feedback);

?>