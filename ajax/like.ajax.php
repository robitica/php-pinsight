<?php 
    session_start();
    
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    $likeStatus = $_POST['likeStatus'];
    $postId = $_POST['postId'];

    // create database connection
    $db = Db::getInstance();
    $user = new User($db);
    $userId = $user->getUserId($_SESSION['username']);

    $post = new Post($db);

    //if user like a post
    if($likeStatus == "liked") {
        $post->likePost($userId, $postId);
    }

    //if user dislike a post
    if($likeStatus == "like") {
        $post->dislikePost($userId, $postId);
    }

    $response = [
        "status"            =>      "success",
        "likeStatus"        =>      $likeStatus,
        "postId"            =>      $postId
    ];

    header('Content-Type: application/json');
    echo json_encode($response);

    ?>