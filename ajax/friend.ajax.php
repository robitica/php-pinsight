<?php 
    session_start();
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    // get data from ajax request
    $inspectedUser = $_POST['inspectedUser'];
    $friendStatus = $_POST['friendStatus'];
    // create database connection
    $db = Db::getInstance();
    // get actual user is and friend user id
    $user = new User($db);
    $userOne = $user->getUserId($_SESSION['username']);
    $userTwo = $user->getUserId($inspectedUser);

    // check btn status
    if($friendStatus == "add-friend") {
        try {
            $newRelationship = new Relationship($db);
            $newRelationship->setUserOne($userOne);
            $newRelationship->setUserTwo($userTwo);
            $newRelationship->addFriendRequest();
            $friendStatus = "pending";
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    } elseif($friendStatus == "accept-friend") {
        try {
            $newRelationship = new Relationship($db);
            $newRelationship->setUserOne($userOne);
            $newRelationship->setUserTwo($userTwo);
            $newRelationship->acceptFriendRequest();
            $friendStatus = "unfriend";
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    } elseif($friendStatus == "unfriend") {
        try {
            $newRelationship = new Relationship($db);
            $newRelationship->setUserOne($userOne);
            $newRelationship->setUserTwo($userTwo);
            $newRelationship->unfriend();
            $friendStatus = "add-friend";
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    }

    $response = [
        "status"            =>      "success",
        "friendUsername"    =>      $inspectedUser,
        "friendStatus"      =>      $friendStatus
    ];

    header('Content-Type: application/json');
    echo json_encode($response);
?>