<?php 
    session_start();
    
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });

    $reportStatus = $_POST['reportStatus']; //gehaald uit data{}
    $postId = $_POST['postId']; //gehaald uit data{}

    // create database connection
    $db = Db::getInstance();
    $user = new User($db);
    $userId = $user->getUserId($_SESSION['username']);

    $post = new Post($db);

    //if user reports a post
    if($reportStatus == "reported") {
        $post->reportPost($userId, $postId); //zet in database
    }

    //if user unReports a post
    if($reportStatus == "report") {
        $post->unReport($userId, $postId);
    }

    $response = [
        "status"            =>      "success",
        "reportStatus"        =>    $reportStatus,
        "postId"            =>      $postId
    ];

    header('Content-Type: application/json');
    echo json_encode($response);

    ?>