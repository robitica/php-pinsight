<?php
    session_start();
    spl_autoload_register(function($class) {
        include_once("../classes/" . $class . ".class.php");
    });
    //get database connection
    $db = Db::getInstance();
    
    $postId = $_POST['postId'];//gehaald uit data{}
    $comment = new Comment($db);
    $user = new User($db);
    $userResults = $user->getUserResults($_SESSION['username']); //get results from commenting user
    $username = $userResults['username'];
    $image = $userResults['avatar'];
    $avatarStatus = $userResults['avatar_status'];
    $id = $user->getUserId($_SESSION['username']); //get user id from current user (session)
    $datePosted = date("Y-m-d H:i:s");
    try{
        $comment->setCommentTxt($_POST['comment']); //set commentTxt to txt from textfield met comment uit data{}
        $comment->setUserId($id);
        $comment->setDate_posted($datePosted);
        $comment->saveComment($postId); //save comment to database
        $feedback['status'] = 'success';
        $feedback['comment'] = htmlspecialchars($_POST['comment']);
        $feedback['username'] = htmlspecialchars($username);
        $feedback['image'] = htmlspecialchars($image);
        $feedback['avatarStatus'] = htmlspecialchars($avatarStatus);
    }
    catch(Exception $e)
    {
        $feedback['status'] = "error";
    }

    //return http JSON header
    header('Content-Type:json/application');
    echo json_encode($feedback);
?>