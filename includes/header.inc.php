<header class="main__header">
    <div class="header__container">
        <a href="index.php" class="main__header__logo"><img src="assets/logo/logo.svg" alt="pinsight logo"></a>
        <nav class="main__nav">
            <ul class="main__nav__original">
                <li class="main__nav__item main__nav__item--home"><a href="index.php"><img src="assets/icons/home.svg" alt="home"></a></li>
                <li class="main__nav__item main__nav__item--profile">
                    <a href="user.php?user=<?php echo $userResults['username'] ?>">
                        <?php
                            if($userResults['avatar_status'] == 0) {
                                echo "<img class='avatar' src='users/avatars/default-profile.png'>";
                            } else {
                                echo User::getAvatar();
                            }
                        ?>
                    </a>
                </li>
                <li class="main__nav__item main__nav__item--upload"><a id="uploadPostBtn" href="upload-post.php"><img src="assets/icons/upload.svg" alt="upload"></a></li>
                <li class="main__nav__item main__nav__item--logout"><a href="settings/logout.php"><img src="assets/icons/logout.svg" alt="logout"></a></li>
                <li class="main__nav__item main__nav__item--search">
                    <form action="search.php" method="get">
                        <label for="search"></label>
                        <input name="search" id="search" type="text" placeholder="Search">
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</header>