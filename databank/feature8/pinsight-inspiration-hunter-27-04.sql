-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 27 apr 2018 om 11:53
-- Serverversie: 10.1.31-MariaDB
-- PHP-versie: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pinsight`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `follow`
--

INSERT INTO `follow` (`id`, `sender`, `receiver`) VALUES
(1, 50, 47),
(2, 49, 47),
(3, 51, 48);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `thumb_image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `posts`
--

INSERT INTO `posts` (`id`, `image`, `thumb_image`, `description`, `title`, `user_id`, `post_time`) VALUES
(1, 'uploads/4k-wallpapers-phone-Is-4K-Wallpaper.jpg', 'uploads/thumbs/thumb-4k-wallpapers-phone-Is-4K-Wallpaper.jpg', 'Description', 'In the night', 49, '2018-04-23 10:47:16'),
(2, 'uploads/228591572-awesome-hd-wallpapers.jpg', 'uploads/thumbs/thumb-228591572-awesome-hd-wallpapers.jpg', 'Description something bla bla bla', 'Boat on the ocean', 47, '2018-04-23 08:00:23'),
(3, 'uploads/6a98d1d4bf37df099b6c74fdf5ee338f--iphone-wallpaper-dark-colors-galaxies-wallpaper.jpg', 'uploads/thumbs/thumb-6a98d1d4bf37df099b6c74fdf5ee338f--iphone-wallpaper-dark-colors-galaxies-wallpaper.jpg', 'Description forest of colors', 'Forest of colors', 48, '2018-04-23 10:44:00'),
(4, 'uploads/55046.jpg', 'uploads/thumbs/thumb-55046.jpg', 'Flower description', 'Flower', 47, '2018-04-23 08:05:38'),
(5, 'uploads/4932669-awesome-hd-wallpapers.jpg', 'uploads/thumbs/thumb-4932669-awesome-hd-wallpapers.jpg', 'Colors description', 'Colors in a glass', 48, '2018-04-23 10:44:03'),
(6, 'uploads/wallpaper17.jpg', 'uploads/thumbs/thumb-wallpaper17.jpg', 'Nature description', 'Nature of life', 47, '2018-04-23 08:08:47'),
(7, 'uploads/general-night-golden-gate-bridge-hd-wallpapers-golden-gate-bridge-wallpaper.jpg', 'uploads/thumbs/thumb-general-night-golden-gate-bridge-hd-wallpapers-golden-gate-bridge-wallpaper.jpg', 'Description city in the night', 'City in the night', 49, '2018-04-23 10:47:19'),
(8, 'uploads/39232043-tumbler-wallpapers.jpg', 'uploads/thumbs/thumb-39232043-tumbler-wallpapers.jpg', 'Flowers in the tree description', 'Flowers in the tree', 49, '2018-04-23 10:47:23'),
(9, 'uploads/Firewatch-Wallpaper-desktop-2560x1440-green.jpg', 'uploads/thumbs/thumb-Firewatch-Wallpaper-desktop-2560x1440-green.jpg', 'Description', 'Green illustration', 50, '2018-04-23 10:47:26'),
(10, 'uploads/HD-Wallpapers-89_FxDoTt7.jpg', 'uploads/thumbs/thumb-HD-Wallpapers-89_FxDoTt7.jpg', 'Ice description', 'Ice', 50, '2018-04-23 10:47:30'),
(11, 'uploads/HD-Wallpapers-C76.jpg', 'uploads/thumbs/thumb-HD-Wallpapers-C76.jpg', 'dark', 'Darkness', 48, '2018-04-23 10:44:07'),
(12, 'uploads/images (1).jpg', 'uploads/thumbs/thumb-images (1).jpg', 'apple', 'Apple', 47, '2018-04-23 08:17:20'),
(13, 'uploads/pexels-photo-443446.jpeg', 'uploads/thumbs/thumb-pexels-photo-443446.jpeg', 'lake description', 'Lake', 50, '2018-04-23 10:47:34'),
(14, 'uploads/images.jpg', 'uploads/thumbs/thumb-images.jpg', 'trees red description', 'Red Trees', 48, '2018-04-23 10:47:41'),
(15, 'uploads/Magic-Forest-Stone-Head-Fantasy-Wallpaper.jpg', 'uploads/thumbs/thumb-Magic-Forest-Stone-Head-Fantasy-Wallpaper.jpg', 'jungle', 'Jungle', 49, '2018-04-23 10:47:37'),
(16, 'uploads/Solar-Eclipse.jpg', 'uploads/thumbs/thumb-Solar-Eclipse.jpg', 'space description', 'Space', 47, '2018-04-23 08:21:14'),
(17, 'uploads/images (2).jpg', 'uploads/thumbs/thumb-images (2).jpg', 'Rain with colors description', 'Rain with colors', 48, '2018-04-23 10:44:12'),
(18, 'uploads/skamath-HD-Wallpapers1.jpg', 'uploads/thumbs/thumb-skamath-HD-Wallpapers1.jpg', 'Coffee description', 'Coffee', 47, '2018-04-23 08:22:42'),
(19, 'uploads/thumb-1920-411820.jpg', 'uploads/thumbs/thumb-thumb-1920-411820.jpg', 'Flowers description', 'Flowrs in the forest', 50, '2018-04-23 10:47:49'),
(20, 'uploads/62832635-smiley-wallpapers.jpg', 'uploads/thumbs/thumb-62832635-smiley-wallpapers.jpg', 'Smily description', 'Smily', 50, '2018-04-23 08:53:42'),
(21, 'uploads/4525621-girls-jacket-wallpapers.jpg', 'uploads/thumbs/thumb-4525621-girls-jacket-wallpapers.jpg', 'Blonde description', 'Blonde', 49, '2018-04-23 10:57:58'),
(22, 'uploads/pexels-photo-568948.jpeg', 'uploads/thumbs/thumb-pexels-photo-568948.jpeg', 'light description', 'Light', 50, '2018-04-23 08:54:50'),
(23, 'uploads/cute-animals-easter-chicken.jpg', 'uploads/thumbs/thumb-cute-animals-easter-chicken.jpg', 'animals description', 'Animals', 47, '2018-04-23 10:57:54'),
(24, 'uploads/Free-Download-Flower-Wallpapers-HD.jpg', 'uploads/thumbs/thumb-Free-Download-Flower-Wallpapers-HD.jpg', 'flower red description', 'Red flower', 47, '2018-04-23 10:57:50'),
(25, 'uploads/pexels-photo-534164.jpeg', 'uploads/thumbs/thumb-pexels-photo-534164.jpeg', 'Nature like home description', 'Nature like home', 50, '2018-04-23 08:56:10'),
(26, 'uploads/pexels-photo-753626.jpeg', 'uploads/thumbs/thumb-pexels-photo-753626.jpeg', 'Holidays description', 'Holidays', 48, '2018-04-23 10:57:46'),
(27, 'uploads/lion-wild-africa-african.jpg', 'uploads/thumbs/thumb-lion-wild-africa-african.jpg', 'Lion king description', 'Lion king', 49, '2018-04-23 10:58:02'),
(28, 'uploads/bereid-uw-konijn-voor-op-koude-dagen-12919.jpg', 'uploads/thumbs/thumb-bereid-uw-konijn-voor-op-koude-dagen-12919.jpg', 'little bunny with a hat', 'Cute bunny', 51, '2018-04-24 18:56:16');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts_likes`
--

CREATE TABLE `posts_likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts_tags`
--

CREATE TABLE `posts_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(13, 1, 17),
(14, 1, 18),
(15, 1, 19),
(16, 1, 20),
(17, 2, 21),
(18, 2, 22),
(19, 2, 23),
(20, 2, 18),
(21, 3, 17),
(22, 3, 24),
(23, 3, 25),
(24, 3, 26),
(25, 4, 27),
(26, 4, 28),
(27, 4, 29),
(28, 5, 24),
(29, 5, 30),
(30, 5, 31),
(31, 5, 32),
(32, 6, 33),
(33, 6, 17),
(34, 6, 32),
(35, 6, 34),
(36, 7, 35),
(37, 7, 36),
(38, 7, 37),
(39, 7, 38),
(40, 8, 17),
(41, 8, 39),
(42, 9, 40),
(43, 9, 41),
(44, 9, 42),
(45, 9, 43),
(46, 9, 20),
(47, 10, 44),
(48, 10, 45),
(49, 10, 46),
(50, 10, 47),
(51, 11, 48),
(52, 11, 49),
(53, 12, 50),
(54, 12, 28),
(55, 12, 51),
(56, 13, 52),
(57, 13, 17),
(58, 13, 31),
(59, 13, 53),
(60, 14, 29),
(61, 14, 17),
(62, 14, 51),
(63, 14, 54),
(64, 15, 55),
(65, 15, 40),
(66, 16, 56),
(67, 16, 57),
(68, 16, 48),
(69, 17, 58),
(70, 17, 24),
(71, 17, 31),
(72, 18, 59),
(73, 19, 60),
(74, 20, 61),
(75, 20, 62),
(76, 20, 63),
(77, 20, 64),
(78, 21, 65),
(79, 21, 66),
(80, 21, 18),
(81, 22, 67),
(82, 22, 68),
(83, 23, 69),
(84, 23, 64),
(85, 24, 27),
(86, 24, 51),
(87, 25, 33),
(88, 25, 70),
(89, 25, 52),
(90, 26, 39),
(91, 26, 71),
(92, 26, 31),
(93, 26, 23),
(94, 27, 69),
(95, 27, 72),
(96, 27, 73),
(97, 28, 74),
(98, 28, 75),
(99, 28, 76);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `relationship`
--

CREATE TABLE `relationship` (
  `id` int(11) NOT NULL,
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `action_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `relationship`
--

INSERT INTO `relationship` (`id`, `user_one_id`, `user_two_id`, `status`, `action_user_id`) VALUES
(1, 49, 50, 1, 50),
(2, 48, 51, 0, 51);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `tags`
--

INSERT INTO `tags` (`id`, `tag_name`) VALUES
(17, 'forest'),
(18, 'night'),
(19, 'deer'),
(20, 'illustration'),
(21, 'boat'),
(22, 'moon'),
(23, 'ocean'),
(24, 'colors'),
(25, 'purple'),
(26, 'love'),
(27, 'flower'),
(28, 'pink'),
(29, 'tree'),
(30, 'glass'),
(31, 'water'),
(32, 'life'),
(33, 'nature'),
(34, 'planet'),
(35, 'bridge'),
(36, 'ciry'),
(37, 'night life'),
(38, 'lights'),
(39, 'summer'),
(40, 'green'),
(41, 'mountains'),
(42, 'vector'),
(43, 'svg'),
(44, 'winter'),
(45, 'blue'),
(46, 'ice'),
(47, 'cold'),
(48, 'dark'),
(49, 'darkness'),
(50, 'apple'),
(51, 'red'),
(52, 'lake'),
(53, 'mountain'),
(54, 'autumn'),
(55, 'jungle'),
(56, 'space'),
(57, 'planets'),
(58, 'rain'),
(59, 'coffee'),
(60, ''),
(61, 'smile'),
(62, 'smily'),
(63, 'happy'),
(64, 'yellow'),
(65, 'blonde'),
(66, 'jacket'),
(67, 'light'),
(68, 'china'),
(69, 'animal'),
(70, 'home'),
(71, 'holiday'),
(72, 'king'),
(73, 'lion'),
(74, 'cute'),
(75, 'food'),
(76, 'fluffy');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_status` int(1) NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `password`, `bio`, `avatar_status`, `avatar`) VALUES
(47, 'Robert Vaida', 'codemaster', 'robertvaida@hotmail.com', '$2y$10$Db4tqqLHS4DoZX8jp3LPsuNzwOPDQ.C6q4fdpJji7yk26CqtVwaC6', 'Ik ben Robert!', 1, 'users/avatars/36e993b3aa5c8aedf42357b05f95eacb.jpg'),
(48, 'George Williek', 'usertest1', 'test1@hotmail.com', '$2y$10$t.2U22U3htNtV8tk7TffR.4EQ3UoJIoawQ2/WqoT0848P8HaIbe5q', '', 1, 'users/avatars/49de0b99449a968e114c7d0b20cf94d8.jpg'),
(49, 'Lili Sander', 'usertest2', 'test2@hotmail.com', '$2y$10$T3kQ6KCxUkGxEgOP7s1EXOv/MFrcsGbQsuH9r35Mpf2ifahW7XKIS', '', 1, 'users/avatars/5bc3f442e2128b2fffd90dfb9d59d701.jpg'),
(50, 'Adeline Clark', 'usertest3', 'test3@hotmail.com', '$2y$10$ixzHvKMWYKnkqg.7K/UX1uuGYFQZYhXlJSVyZ1ph7FpqPQBWTP.MO', '', 1, 'users/avatars/bf71396be5d50ba5c6996dca55e70db9.jpg'),
(52, 'Michelle Schultz', 'evestudio', 'michelle.eve.schultz@gmail.com', '$2y$10$1Db9VBn4skUPI2o2Swg7N.OpcaBzi04lfl4Fx6rZOlqajfjZMmnK6', '', 0, '');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `posts_likes`
--
ALTER TABLE `posts_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `relationship`
--
ALTER TABLE `relationship`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT voor een tabel `posts_likes`
--
ALTER TABLE `posts_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT voor een tabel `posts_tags`
--
ALTER TABLE `posts_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT voor een tabel `relationship`
--
ALTER TABLE `relationship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
