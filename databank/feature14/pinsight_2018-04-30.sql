# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.35)
# Database: pinsight
# Generation Time: 2018-04-30 21:38:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `text`, `date_posted`)
VALUES
	(128,54,27,'dit is een comment','2018-04-29 17:47:03'),
	(129,54,27,'wow wat een mooie leeuw','2018-04-29 17:48:28'),
	(130,54,27,'f','2018-04-29 17:50:52'),
	(131,54,27,'','2018-04-29 17:51:50'),
	(132,54,27,'f','2018-04-29 17:51:59'),
	(133,54,27,'','2018-04-29 17:52:52'),
	(134,54,27,'','2018-04-29 17:53:47'),
	(135,54,27,'','2018-04-29 17:54:10'),
	(136,54,27,'','2018-04-29 17:55:32'),
	(137,54,27,'','2018-04-29 17:55:33'),
	(138,54,27,'','2018-04-29 17:55:33'),
	(139,54,27,'','2018-04-29 17:55:33'),
	(140,54,27,'','2018-04-29 17:55:36'),
	(141,54,27,'fds','2018-04-29 17:55:56'),
	(142,53,27,'','2018-04-29 17:58:18'),
	(143,53,27,'','2018-04-29 17:59:12'),
	(144,53,27,'','2018-04-29 18:00:30'),
	(145,53,27,'','2018-04-29 18:03:28'),
	(146,53,27,'','2018-04-29 18:03:51'),
	(147,53,27,'','2018-04-29 18:05:56'),
	(148,53,27,'','2018-04-29 18:06:39'),
	(149,53,27,'','2018-04-29 18:08:09'),
	(150,53,27,'','2018-04-29 18:11:32'),
	(151,53,27,'','2018-04-29 18:15:01'),
	(152,53,27,'','2018-04-29 18:45:33'),
	(153,53,27,'fdsf','2018-04-29 18:45:59'),
	(154,53,27,'','2018-04-29 18:46:25'),
	(155,53,27,'','2018-04-29 18:51:44'),
	(156,53,27,'','2018-04-29 18:55:01'),
	(157,53,27,'','2018-04-29 18:55:02'),
	(158,53,27,'fds','2018-04-29 18:55:06'),
	(159,53,27,'','2018-04-29 18:56:12'),
	(160,53,27,'','2018-04-29 18:56:26'),
	(161,53,27,'','2018-04-29 18:59:26');

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table follow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `follow`;

CREATE TABLE `follow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;

INSERT INTO `follow` (`id`, `sender`, `receiver`)
VALUES
	(1,50,47),
	(2,49,47),
	(3,51,48);

/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL DEFAULT '',
  `thumb_image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;

INSERT INTO `posts` (`id`, `image`, `thumb_image`, `description`, `title`, `user_id`, `post_time`)
VALUES
	(1,'uploads/4k-wallpapers-phone-Is-4K-Wallpaper.jpg','uploads/thumbs/thumb-4k-wallpapers-phone-Is-4K-Wallpaper.jpg','Description','In the night',49,'2018-04-23 12:47:16'),
	(2,'uploads/228591572-awesome-hd-wallpapers.jpg','uploads/thumbs/thumb-228591572-awesome-hd-wallpapers.jpg','Description something bla bla bla','Boat on the ocean',47,'2018-04-23 10:00:23'),
	(3,'uploads/6a98d1d4bf37df099b6c74fdf5ee338f--iphone-wallpaper-dark-colors-galaxies-wallpaper.jpg','uploads/thumbs/thumb-6a98d1d4bf37df099b6c74fdf5ee338f--iphone-wallpaper-dark-colors-galaxies-wallpaper.jpg','Description forest of colors','Forest of colors',48,'2018-04-23 12:44:00'),
	(4,'uploads/55046.jpg','uploads/thumbs/thumb-55046.jpg','Flower description','Flower',47,'2018-04-23 10:05:38'),
	(5,'uploads/4932669-awesome-hd-wallpapers.jpg','uploads/thumbs/thumb-4932669-awesome-hd-wallpapers.jpg','Colors description','Colors in a glass',48,'2018-04-23 12:44:03'),
	(6,'uploads/wallpaper17.jpg','uploads/thumbs/thumb-wallpaper17.jpg','Nature description','Nature of life',47,'2018-04-23 10:08:47'),
	(7,'uploads/general-night-golden-gate-bridge-hd-wallpapers-golden-gate-bridge-wallpaper.jpg','uploads/thumbs/thumb-general-night-golden-gate-bridge-hd-wallpapers-golden-gate-bridge-wallpaper.jpg','Description city in the night','City in the night',49,'2018-04-23 12:47:19'),
	(8,'uploads/39232043-tumbler-wallpapers.jpg','uploads/thumbs/thumb-39232043-tumbler-wallpapers.jpg','Flowers in the tree description','Flowers in the tree',49,'2018-04-23 12:47:23'),
	(9,'uploads/Firewatch-Wallpaper-desktop-2560x1440-green.jpg','uploads/thumbs/thumb-Firewatch-Wallpaper-desktop-2560x1440-green.jpg','Description','Green illustration',50,'2018-04-23 12:47:26'),
	(10,'uploads/HD-Wallpapers-89_FxDoTt7.jpg','uploads/thumbs/thumb-HD-Wallpapers-89_FxDoTt7.jpg','Ice description','Ice',50,'2018-04-23 12:47:30'),
	(11,'uploads/HD-Wallpapers-C76.jpg','uploads/thumbs/thumb-HD-Wallpapers-C76.jpg','dark','Darkness',48,'2018-04-23 12:44:07'),
	(12,'uploads/images (1).jpg','uploads/thumbs/thumb-images (1).jpg','apple','Apple',47,'2018-04-23 10:17:20'),
	(13,'uploads/pexels-photo-443446.jpeg','uploads/thumbs/thumb-pexels-photo-443446.jpeg','lake description','Lake',50,'2018-04-23 12:47:34'),
	(14,'uploads/images.jpg','uploads/thumbs/thumb-images.jpg','trees red description','Red Trees',48,'2018-04-23 12:47:41'),
	(15,'uploads/Magic-Forest-Stone-Head-Fantasy-Wallpaper.jpg','uploads/thumbs/thumb-Magic-Forest-Stone-Head-Fantasy-Wallpaper.jpg','jungle','Jungle',49,'2018-04-23 12:47:37'),
	(16,'uploads/Solar-Eclipse.jpg','uploads/thumbs/thumb-Solar-Eclipse.jpg','space description','Space',47,'2018-04-23 10:21:14'),
	(17,'uploads/images (2).jpg','uploads/thumbs/thumb-images (2).jpg','Rain with colors description','Rain with colors',48,'2018-04-23 12:44:12'),
	(18,'uploads/skamath-HD-Wallpapers1.jpg','uploads/thumbs/thumb-skamath-HD-Wallpapers1.jpg','Coffee description','Coffee',47,'2018-04-23 10:22:42'),
	(19,'uploads/thumb-1920-411820.jpg','uploads/thumbs/thumb-thumb-1920-411820.jpg','Flowers description','Flowrs in the forest',50,'2018-04-23 12:47:49'),
	(20,'uploads/62832635-smiley-wallpapers.jpg','uploads/thumbs/thumb-62832635-smiley-wallpapers.jpg','Smily description','Smily',50,'2018-04-23 10:53:42'),
	(21,'uploads/4525621-girls-jacket-wallpapers.jpg','uploads/thumbs/thumb-4525621-girls-jacket-wallpapers.jpg','Blonde description','Blonde',49,'2018-04-23 12:57:58'),
	(22,'uploads/pexels-photo-568948.jpeg','uploads/thumbs/thumb-pexels-photo-568948.jpeg','light description','Light',50,'2018-04-23 10:54:50'),
	(23,'uploads/cute-animals-easter-chicken.jpg','uploads/thumbs/thumb-cute-animals-easter-chicken.jpg','animals description','Animals',47,'2018-04-23 12:57:54'),
	(24,'uploads/Free-Download-Flower-Wallpapers-HD.jpg','uploads/thumbs/thumb-Free-Download-Flower-Wallpapers-HD.jpg','flower red description','Red flower',47,'2018-04-23 12:57:50'),
	(25,'uploads/pexels-photo-534164.jpeg','uploads/thumbs/thumb-pexels-photo-534164.jpeg','Nature like home description','Nature like home',50,'2018-04-23 10:56:10'),
	(26,'uploads/pexels-photo-753626.jpeg','uploads/thumbs/thumb-pexels-photo-753626.jpeg','Holidays description','Holidays',48,'2018-04-23 12:57:46'),
	(27,'uploads/lion-wild-africa-african.jpg','uploads/thumbs/thumb-lion-wild-africa-african.jpg','Lion king description','Lion king',49,'2018-04-23 12:58:02'),
	(28,'uploads/bereid-uw-konijn-voor-op-koude-dagen-12919.jpg','uploads/thumbs/thumb-bereid-uw-konijn-voor-op-koude-dagen-12919.jpg','little bunny with a hat','Cute bunny',51,'2018-04-24 20:56:16'),
	(29,'uploads/November_Newsletter-01.png','uploads/thumbs/thumb-November_Newsletter-01.png','Dont waste food','Food waste',56,'2018-04-30 14:32:10'),
	(30,'uploads/biggorg1.jpg','uploads/thumbs/thumb-biggorg1.jpg','XYZ','X',56,'2018-04-30 19:10:04'),
	(31,'uploads/Cute-Birds-Image-1024x575.jpg','uploads/thumbs/thumb-Cute-Birds-Image-1024x575.jpg','YY','XX',56,'2018-04-30 19:10:42'),
	(32,'uploads/adw66.jpg','uploads/thumbs/thumb-adw66.jpg','JKJ','HHH',56,'2018-04-30 19:11:51'),
	(33,'uploads/adw14.jpg','uploads/thumbs/thumb-adw14.jpg','YYY','XX',56,'2018-04-30 20:47:28'),
	(34,'uploads/adw14.jpg','uploads/thumbs/thumb-adw14.jpg','YY','XX',56,'2018-04-30 20:48:36'),
	(35,'uploads/adw66.jpg','uploads/thumbs/thumb-adw66.jpg','save`','AEDofdo',56,'2018-04-30 20:49:40'),
	(36,'uploads/biggorg1.jpg','uploads/thumbs/thumb-biggorg1.jpg','scadvsa','sacsa',56,'2018-04-30 20:51:29'),
	(39,'uploads/adw14.jpg','uploads/thumbs/thumb-adw14.jpg','test','tet',56,'2018-04-30 22:15:34');

/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts_colors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts_colors`;

CREATE TABLE `posts_colors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `hex` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table posts_likes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts_likes`;

CREATE TABLE `posts_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts_likes` WRITE;
/*!40000 ALTER TABLE `posts_likes` DISABLE KEYS */;

INSERT INTO `posts_likes` (`id`, `post_id`, `user_id`)
VALUES
	(177,27,53),
	(178,26,56),
	(179,27,56);

/*!40000 ALTER TABLE `posts_likes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts_reports
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts_reports`;

CREATE TABLE `posts_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table posts_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts_tags`;

CREATE TABLE `posts_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `posts_tags` WRITE;
/*!40000 ALTER TABLE `posts_tags` DISABLE KEYS */;

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`)
VALUES
	(13,1,17),
	(14,1,18),
	(15,1,19),
	(16,1,20),
	(17,2,21),
	(18,2,22),
	(19,2,23),
	(20,2,18),
	(21,3,17),
	(22,3,24),
	(23,3,25),
	(24,3,26),
	(25,4,27),
	(26,4,28),
	(27,4,29),
	(28,5,24),
	(29,5,30),
	(30,5,31),
	(31,5,32),
	(32,6,33),
	(33,6,17),
	(34,6,32),
	(35,6,34),
	(36,7,35),
	(37,7,36),
	(38,7,37),
	(39,7,38),
	(40,8,17),
	(41,8,39),
	(42,9,40),
	(43,9,41),
	(44,9,42),
	(45,9,43),
	(46,9,20),
	(47,10,44),
	(48,10,45),
	(49,10,46),
	(50,10,47),
	(51,11,48),
	(52,11,49),
	(53,12,50),
	(54,12,28),
	(55,12,51),
	(56,13,52),
	(57,13,17),
	(58,13,31),
	(59,13,53),
	(60,14,29),
	(61,14,17),
	(62,14,51),
	(63,14,54),
	(64,15,55),
	(65,15,40),
	(66,16,56),
	(67,16,57),
	(68,16,48),
	(69,17,58),
	(70,17,24),
	(71,17,31),
	(72,18,59),
	(73,19,60),
	(74,20,61),
	(75,20,62),
	(76,20,63),
	(77,20,64),
	(78,21,65),
	(79,21,66),
	(80,21,18),
	(81,22,67),
	(82,22,68),
	(83,23,69),
	(84,23,64),
	(85,24,27),
	(86,24,51),
	(87,25,33),
	(88,25,70),
	(89,25,52),
	(90,26,39),
	(91,26,71),
	(92,26,31),
	(93,26,23),
	(94,27,69),
	(95,27,72),
	(96,27,73),
	(97,28,74),
	(98,28,75),
	(99,28,76),
	(100,29,77),
	(101,30,78),
	(102,31,79),
	(103,32,80),
	(104,31,81),
	(105,31,79),
	(106,35,82),
	(107,36,83),
	(108,37,84),
	(109,38,84),
	(110,39,85);

/*!40000 ALTER TABLE `posts_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table relationship
# ------------------------------------------------------------

DROP TABLE IF EXISTS `relationship`;

CREATE TABLE `relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `action_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `relationship` WRITE;
/*!40000 ALTER TABLE `relationship` DISABLE KEYS */;

INSERT INTO `relationship` (`id`, `user_one_id`, `user_two_id`, `status`, `action_user_id`)
VALUES
	(1,49,50,1,50),
	(2,48,51,0,51);

/*!40000 ALTER TABLE `relationship` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;

INSERT INTO `tags` (`id`, `tag_name`)
VALUES
	(17,'forest'),
	(18,'night'),
	(19,'deer'),
	(20,'illustration'),
	(21,'boat'),
	(22,'moon'),
	(23,'ocean'),
	(24,'colors'),
	(25,'purple'),
	(26,'love'),
	(27,'flower'),
	(28,'pink'),
	(29,'tree'),
	(30,'glass'),
	(31,'water'),
	(32,'life'),
	(33,'nature'),
	(34,'planet'),
	(35,'bridge'),
	(36,'ciry'),
	(37,'night life'),
	(38,'lights'),
	(39,'summer'),
	(40,'green'),
	(41,'mountains'),
	(42,'vector'),
	(43,'svg'),
	(44,'winter'),
	(45,'blue'),
	(46,'ice'),
	(47,'cold'),
	(48,'dark'),
	(49,'darkness'),
	(50,'apple'),
	(51,'red'),
	(52,'lake'),
	(53,'mountain'),
	(54,'autumn'),
	(55,'jungle'),
	(56,'space'),
	(57,'planets'),
	(58,'rain'),
	(59,'coffee'),
	(60,''),
	(61,'smile'),
	(62,'smily'),
	(63,'happy'),
	(64,'yellow'),
	(65,'blonde'),
	(66,'jacket'),
	(67,'light'),
	(68,'china'),
	(69,'animal'),
	(70,'home'),
	(71,'holiday'),
	(72,'king'),
	(73,'lion'),
	(74,'cute'),
	(75,'food'),
	(76,'fluffy'),
	(77,'foodwaste'),
	(78,'x'),
	(79,'zz'),
	(80,'jj'),
	(81,'zzz'),
	(82,'safbv'),
	(83,'sdv'),
	(84,'test'),
	(85,'tt');

/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_status` int(1) NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `password`, `bio`, `avatar_status`, `avatar`)
VALUES
	(47,'Robert Vaida','codemaster','robertvaida@hotmail.com','$2y$10$Db4tqqLHS4DoZX8jp3LPsuNzwOPDQ.C6q4fdpJji7yk26CqtVwaC6','Ik ben Robert!',1,'users/avatars/36e993b3aa5c8aedf42357b05f95eacb.jpg'),
	(48,'George Williek','usertest1','test1@hotmail.com','$2y$10$t.2U22U3htNtV8tk7TffR.4EQ3UoJIoawQ2/WqoT0848P8HaIbe5q','',1,'users/avatars/49de0b99449a968e114c7d0b20cf94d8.jpg'),
	(49,'Lili Sander','usertest2','test2@hotmail.com','$2y$10$T3kQ6KCxUkGxEgOP7s1EXOv/MFrcsGbQsuH9r35Mpf2ifahW7XKIS','',1,'users/avatars/5bc3f442e2128b2fffd90dfb9d59d701.jpg'),
	(50,'Adeline Clark','usertest3','test3@hotmail.com','$2y$10$ixzHvKMWYKnkqg.7K/UX1uuGYFQZYhXlJSVyZ1ph7FpqPQBWTP.MO','',1,'users/avatars/bf71396be5d50ba5c6996dca55e70db9.jpg'),
	(52,'Michelle Schultz','evestudio','michelle.eve.schultz@gmail.com','$2y$10$1Db9VBn4skUPI2o2Swg7N.OpcaBzi04lfl4Fx6rZOlqajfjZMmnK6','',0,''),
	(53,'louizavanherp','louizavanherp','louiza.vanherp@gmail.com','$2y$10$kQNYYJz7D9jUdScM3gANIO9PXGdwN13VWHFqCIhv9gq9Z71XpEKwS','',0,''),
	(54,'emma','emma','emma@emma.com','$2y$10$/ub0.wNF0wglW.a/GiTRjOruC05mgJrbXdiyWi7QtkuV.EsJfkrVm','',1,'users/avatars/00a809937eddc44521da9521269e75c6.jpg'),
	(55,'wim','wim','wim@wim.vanherp','$2y$10$O7frudTJ2SeXKtLl0qHJIe6T47hang18YVRHQ601JqgC1qkOgPcty','',0,''),
	(56,'Bibi Treffers','bix2','bibi.treffers@outlook.com','$2y$10$KFrpzdjCSIYzMN5csarg4OWnEeO1u2FlDgsnKZS32n2jTJjyBZNcm','',1,'users/avatars/c22b4d71246577df1c4235567337d271.jpg');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
