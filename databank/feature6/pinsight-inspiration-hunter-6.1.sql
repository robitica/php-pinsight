-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 22 apr 2018 om 15:52
-- Serverversie: 10.1.31-MariaDB
-- PHP-versie: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pinsight`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `follow`
--

INSERT INTO `follow` (`id`, `sender`, `receiver`) VALUES
(12, 43, 44),
(30, 44, 41),
(31, 45, 44),
(32, 42, 43),
(33, 41, 44);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts`
--

CREATE TABLE `posts` (
  `id` int(11) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `thumb_image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Gegevens worden geëxporteerd voor tabel `posts`
--

INSERT INTO `posts` (`id`, `image`, `thumb_image`, `description`, `user_id`, `post_time`, `title`) VALUES
(7, 'uploads/the-jungle.jpg', 'uploads/thumbs/thumb-the-jungle.jpg', 'Voila', 44, '2018-04-11 05:53:43', NULL),
(8, 'uploads/pexels-photo-457882.jpeg', 'uploads/thumbs/thumb-pexels-photo-457882.jpeg', 'Hello World', 41, '2018-04-11 05:53:54', NULL),
(9, 'uploads/blog_072117_house-senate-vote-horse-slaughter_main_1.jpg', 'uploads/thumbs/thumb-blog_072117_house-senate-vote-horse-slaughter_main_1.jpg', 'Hellow World', 41, '2018-04-11 05:32:01', NULL),
(10, 'uploads/Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg', 'uploads/thumbs/thumb-Simplest-Responsive-jQuery-Image-Lightbox-Plugin-simple-lightbox.jpg', 'dsadsa', 42, '2018-04-11 05:32:01', NULL),
(11, 'uploads/unnamed.jpg', 'uploads/thumbs/thumb-unnamed.jpg', 'sadsad', 42, '2018-04-11 05:32:01', NULL),
(12, 'uploads/blog_072117_house-senate-vote-horse-slaughter_main_1.jpg', 'uploads/thumbs/thumb-blog_072117_house-senate-vote-horse-slaughter_main_1.jpg', 'dsadasd', 43, '2018-04-11 05:32:01', NULL),
(13, 'uploads/ourHistory_retina.jpg', 'uploads/thumbs/thumb-ourHistory_retina.jpg', 'asdsadsada', 41, '2018-04-11 05:32:01', NULL),
(14, 'uploads/min-kyung-6.jpg', 'uploads/thumbs/thumb-min-kyung-6.jpg', 'dsaas', 44, '2018-04-11 05:32:01', NULL),
(15, 'uploads/min-kyung-2.jpg', 'uploads/thumbs/thumb-min-kyung-2.jpg', 'sadsad', 44, '2018-04-11 05:32:01', NULL),
(16, 'uploads/min-kyung.jpg', 'uploads/thumbs/thumb-min-kyung.jpg', 'sadsafsafa', 44, '2018-04-11 05:32:01', NULL),
(19, 'uploads/emailflights_flight_ss_1.jpg', 'uploads/thumbs/thumb-emailflights_flight_ss_1.jpg', 'sadad', 43, '2018-04-11 05:32:01', NULL),
(20, 'uploads/the-jungle.jpg', 'uploads/thumbs/thumb-the-jungle.jpg', 'sada', 43, '2018-04-11 05:32:01', NULL),
(21, 'uploads/134345-OSIGJ9-67.png', 'uploads/thumbs/thumb-134345-OSIGJ9-67.png', 'Hello', 43, '2018-04-16 04:19:30', NULL),
(22, 'uploads/biggorg1.jpg', 'uploads/thumbs/thumb-biggorg1.jpg', 'A nice woman', 46, '2018-04-21 10:56:27', 'Woman'),
(23, 'uploads/Funny-Cat-Glasses-Wallpaper-1024x640.jpg', 'uploads/thumbs/thumb-Funny-Cat-Glasses-Wallpaper-1024x640.jpg', 'This is a cool cat with sunglasses on', 46, '2018-04-21 10:56:47', 'Cool kittie'),
(24, 'uploads/21720-guitar-on-the-bed-1680x1050-music-wallpaper.jpg', 'uploads/thumbs/thumb-21720-guitar-on-the-bed-1680x1050-music-wallpaper.jpg', 'Guitar on the bed', 46, '2018-04-21 10:57:02', 'Guitar');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts_tags`
--

CREATE TABLE `posts_tags` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(8, 22, 51),
(9, 22, 52),
(10, 22, 53),
(11, 23, 54),
(12, 23, 55),
(13, 24, 56),
(14, 24, 57),
(15, 24, 58);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `relationship`
--

CREATE TABLE `relationship` (
  `id` int(11) NOT NULL,
  `user_one_id` int(11) NOT NULL,
  `user_two_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `action_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `tags`
--

INSERT INTO `tags` (`id`, `tag_name`) VALUES
(51, 'woman'),
(52, 'art'),
(53, 'graphicdesign'),
(54, 'cat'),
(55, 'sunglasses'),
(56, 'guitar'),
(57, 'acoustic'),
(58, 'music');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_status` int(1) NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `password`, `bio`, `avatar_status`, `avatar`) VALUES
(41, 'Bibi Treffers', 'bix2', 'bibi@test.com', '$2y$10$RBx.sPj9SmwhHn/2rnzAtuIBhtgBd9QXl8Ani/kjHa53.r4tPZa8.', 'test', 1, 'users/avatars/c22b4d71246577df1c4235567337d271.jpg'),
(42, 'Louiza Vanherp', 'louiza', 'loulou@test.com', '$2y$10$mmNh0CEUNyV4o9eMQt1lZ.qYNgYomnlormRyf5nwSZ1qa5LrE5wUS', '', 1, 'users/avatars/d6322af44b56c6802e81bfcd837f7503.jpg'),
(43, 'Robert Vaida', 'codemaster', 'robertvaida@hotmail.com', '$2y$10$VEgadzCj0ywS3mkZL3h1YOZazSXNNKMiiQq6hvUaaWOGaYgRmmOeS', 'Hello! Ik ben Robert en ik ben niet zeer slim...', 1, 'users/avatars/5318f41aeef98fcf81ba7c9a388a263a.jpg'),
(44, 'Michelle Eve', 'michi', 'michi@test.com', '$2y$10$ynNoE9GlUCL8rw.A8Ad2BeFIFkGi6Q2/FNXO4rhr0LisU.0jIWJty', '', 1, 'users/avatars/83c34e55e78b3f416b091594fc8b6e76.jpg'),
(45, 'Robby Bobby', 'robbybobby', 'vaidarobert@hotmail.com', '$2y$10$nGDs628dTJlVvw5NrvCg9eYiGqk.jcldtST9WEJrOHXGHzEFlTwpW', '', 0, ''),
(46, 'bibi', 'bibs', 'bibi@bibi.com', '$2y$10$4S6uIbHe12Q4S4p8FqawqurUP.d73QpWIMhTbFSQ5tNmAeFem/5Jq', '', 0, '');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `posts_tags`
--
ALTER TABLE `posts_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `relationship`
--
ALTER TABLE `relationship`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT voor een tabel `posts_tags`
--
ALTER TABLE `posts_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT voor een tabel `relationship`
--
ALTER TABLE `relationship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
