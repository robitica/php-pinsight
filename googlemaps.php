<?php
 
    session_start();
    spl_autoload_register(function($class) {
        include_once("classes/" . $class . ".class.php");
    });


    // check if logged in
    User::authenticate();
   
    // create database connection
    $db = Db::getInstance();

    // get user informations to use on posts (username, avatar, etc.)
    $user = new User($db);
    $userResults = $user->getUserResults($_SESSION['username']);

    // posts
    $posts = new Post($db);
    
    if (isset($_GET['filter'])) {
        // check if search input exists
        if(empty($_GET['filter'])) {
            $markers = $posts->getAllPosts();
        }

        $filter = $_GET["filter"];
        $param = str_replace('#', '', $filter);
        try {
            $results = $posts->filterByTags($param);
        } catch(Exception $e) {
            $error = $e->getMessage();
        }
    };

?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinsight</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cssgram/0.1.10/cssgram.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<style>
   #map {
        height: 90%;
    }

    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
</style>
<body>

    <!-- navigation -->
    <?php
        include_once("includes/header.inc.php");
    ?>
    
    <!-- secondary menu -->
    <nav class="secondary__nav">
        <ul>
            <li class="<?php echo (!empty($pageName) && $pageName == 'followers') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php?sort=followers">Following</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'friends') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php?sort=friends">Friends</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'index') ? 'secondary__nav__item secondary__nav__item--selected' : 'secondary__nav__item'; ?>"><a href="index.php">All</a></li>
            <li class="<?php echo (!empty($pageName) && $pageName == 'nearby') ? 'secondary__nav__item nearby secondary__nav__item--selected' : 'secondary__nav__item nearby'; ?>"><a href="index.php?sort=nearby">Nearby</a></li>
            <li class="googlemaps secondary__nav__item secondary__nav__item--selected"><a href="googlemaps.php">Map</a></li>
        </ul>
    </nav>

    <!-- filter form for map -->
    <div class="map__search">
        <div class="flexsearch">
            <div class="flexsearch--wrapper">
                <form class="flexsearch--form" action="" method="post">
                    <div class="flexsearch--input-wrapper">
                        <input class="flexsearch--input filter__data" name="filter" type="text" placeholder="search">
                    </div>
                    <a href="#" class="flexsearch--submit filter__on__map">&#10140;</a>
                </form>
            </div>
        </div>
    </div>
    <!-- MAP -->
    <div id="map"></div>

    <!-- Footer -->
    <?php
        include_once("includes/footer.inc.php");
    ?>
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/markerclusterer.js"></script>
    <script>
    // define markers
    var markers = [];
    var markerCluster;

    // initialize map
    function initMap() {
        var myLatLng = new google.maps.LatLng(20.9658417, 4.457731);
        var mapOptions = {
            zoom: 10,
            center: myLatLng
        }
        var map = new google.maps.Map(document.getElementById('map'), mapOptions);

        // HTML5 geolocation for zoom settings
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                map.setCenter(pos);
            });
        }

        // define infowindow
        var infowindow = [];

        // when window finishes loading
        $(window).on('load', function () {
            // carry out an ajax request
            $.ajax({
                url: 'ajax/addMarkers.ajax.php'
            })
            .done(function(res){
                for (var i = 0; i < res.locations.length; i++) {
                        var pos = new google.maps.LatLng(res.locations[i].latitude, res.locations[i].longitude);
                        var newmarker = new google.maps.Marker({
                            map: map,
                            position: pos
                        });

                        // infowindow when clicking on marker
                        var content = `<div class="marker-content"><a href="sight.php?id=${res.locations[i].id}"><img width='40px' src="${res.locations[i].thumb_image}"></a></div>`;
                        newmarker['infowindow'] = new google.maps.InfoWindow({
                            content: content
                        })

                        google.maps.event.addListener(newmarker, 'click', function(){
                            this['infowindow'].open(map, this);
                        });

                        markers.push(newmarker);
                    }
              markerCluster = new MarkerClusterer(map, markers, { imagePath: 'assets/mapicons/m' });
            })        
        });

        $(".flexsearch--form").submit(function (e) {
            clearMap();

            var filter = $(".filter__data").val();

            $.ajax({
                method: 'POST',
                url: 'ajax/addMarkers.ajax.php',
                data: {filter: filter}
            })
            .done(function(res){
                console.log(res);
                for (var i = 0; i < res.locations.length; i++) {
                        var pos = new google.maps.LatLng(res.locations[i].latitude, res.locations[i].longitude);
                        var newmarker = new google.maps.Marker({
                            map: map,
                            position: pos
                        });

                        // infowindow when clicking on marker
                        var content = `<div class="marker-content"><a href="sight.php?id=${res.locations[i].id}"><img width='40px' src="${res.locations[i].thumb_image}"></a></div>`;
                        newmarker['infowindow'] = new google.maps.InfoWindow({
                            content: content
                        })

                        google.maps.event.addListener(newmarker, 'click', function(){
                            this['infowindow'].open(map, this);
                        });

                        markers.push(newmarker);
                }

                $(".flexsearch--form").trigger("reset");
                markerCluster = new MarkerClusterer(map, markers, { imagePath: 'assets/mapicons/m' });
            }) 
           
            e.preventDefault();       
        })
    };

    function clearMap() {
       /* if (markers) {
            for (i in markers) {
            markers[i].setMap(null);
            }
        }*/
        for (i = 0; i < markers.length; i++) {
            markers[i].setMap(null)
        }
        markers = [];

        // Clears all clusters and markers from the clusterer.
        markerCluster.clearMarkers();
    }
 
        

    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZCbtp4wFaT5EzpdudCaE76Grj3sTu9e4&callback=initMap"></script>
 
</body>
</html>