<?php

    class Image {
        private $file;
        private $fileTmpName;
        private $fileName;
        private $fileSize;
        private $fileError;
        private $fileType;

        /**
         * Get the value of file
         */ 
        public function getFile() {
            return $this->file;
        }

        /**
         * Set the value of file
         * @return  self
         */ 
        public function setFile($file) {
            $this->file = $file;
            return $this;
        }

        /**
         * Get the value of fileTmpName
         */ 
        public function getFileTmpName() {
            return $this->fileTmpName;
        }

        /**
         * Set the value of fileTmpName
         * @return  self
         */ 
        public function setFileTmpName($fileTmpName) {
            $this->fileTmpName = $fileTmpName;
            return $this;
        }

        /**
         * Get the value of fileName
         */ 
        public function getFileName() {
            return $this->fileName;
        }

        /**
         * Set the value of fileName
         * @return  self
         */ 
        public function setFileName($fileName) {
            $this->fileName = $fileName;
            return $this;
        }

        /**
         * Get the value of fileSize
         */ 
        public function getFileSize() {
            return $this->fileSize;
        }

        /**
         * Set the value of fileSize
         * @return  self
         */ 
        public function setFileSize($fileSize) {
            // check if the size of the uploaded image is smaller than 800kb
            if ($this->fileSize > 800000) {
                throw new Exception("Your file is too big! 800kb max");
            }
            $this->fileSize = $fileSize;
            return $this;
        }

        /**
         * Get the value of fileError
         */ 
        public function getFileError() {
            return $this->fileError;
        }

        /**
         * Set the value of fileError
         * @return  self
         */ 
        public function setFileError($fileError) {
            // check also if there is no error
            if ($this->fileError === 0) {
                throw new Exception("There was an error uploading your file!");
            }
            $this->fileError = $fileError;
            return $this;
        }

        /**
         * Get the value of fileType
         */ 
        public function getFileType() {
            return $this->fileType;
        }

        /**
         * Set the value of fileType
         * @return  self
         */ 
        public function setFileType($fileType) {
            $this->fileType = $fileType;
            return $this;
        }

        public function resizeCropImage($max_width, $max_height, $source_file, $dst_dir, $quality = 80) {
            $imgsize = getimagesize($source_file);
            $width = $imgsize[0];
            $height = $imgsize[1];
            $mime = $imgsize['mime'];
         
            switch($mime){
                case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;
         
                case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 80;
                break;
         
                default:
                return false;
                break;
            }
             
            $dst_img = imagecreatetruecolor($max_width, $max_height);
            $src_img = $image_create($source_file);
             
            $width_new = $height * $max_width / $max_height;
            $height_new = $width * $max_height / $max_width;
            //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
            if($width_new > $width){
                //cut point by height
                $h_point = (($height - $height_new) / 2);
                //copy image
                imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
            }else{
                //cut point by width
                $w_point = (($width - $width_new) / 2);
                imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
            }
            
                // integer representation of the color black (rgb: 0,0,0)
                $background = imagecolorallocate($dst_img , 0, 0, 0);
                // removing the black from the placeholder
                imagecolortransparent($dst_img, $background);
                $image($dst_img, $dst_dir, $quality);
                
                if($dst_img)imagedestroy($dst_img);
                if($src_img)imagedestroy($src_img);
        }

        public function getActualFileExtention() {
            $image_path = "users/avatars" . basename($this->getFileName());
            // get the extention from the uploaded image (.jpg, .png, etc)
            $mime = pathinfo($image_path,PATHINFO_EXTENSION);
            // bring the extention to lowercases (because .jpg can also be .JPG)
            $fileActualExt = strtolower($mime);
            // return file extention
            return $fileActualExt;
        }

        public function getFileNewName($fileActualExt) {
            // prevent that 2 files with the same name will override each other
            $fileNameNew = md5($_SESSION['username']).".".$fileActualExt;
            return $fileNameNew;
        }
    }

?>